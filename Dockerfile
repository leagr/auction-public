FROM node AS builder

ARG REACT_APP_API_URL

WORKDIR /app

COPY . ./

RUN npm install

RUN REACT_APP_API_URL=$REACT_APP_API_URL npm run build



FROM nginx:alpine

COPY --from=builder /app/build /usr/share/nginx/html

COPY ./default.conf /etc/nginx/conf.d/default.conf
