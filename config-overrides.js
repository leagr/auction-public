/* eslint-disable */
/* config-overrides.js */
const { injectBabelPlugin, getBabelLoader } = require('react-app-rewired');

module.exports = function override(config, env) {
  const babelLoader = getBabelLoader(config.module.rules);
  config = injectBabelPlugin('transform-decorators-legacy', config);

  config.module.rules.map(rule => {
    if (typeof rule.test !== 'undefined' || typeof rule.oneOf === 'undefined') {
      return rule;
    }

    rule.oneOf.unshift({
      test: /\.mdx$/,
      use: [
        {
          loader: babelLoader.loader,
          options: babelLoader.options,
        },
        '@mdx-js/loader',
      ],
    });

    return rule;
  });

  return config;
};
