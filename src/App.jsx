import React from 'react';
import { Provider } from 'mobx-react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core';
import blue from '@material-ui/core/colors/blue';
import 'moment/locale/ru';
import 'typeface-roboto/index.css';
// import DevTools from 'mobx-react-devtools';

import './index.css';
import API from './api';
import {
  ErrorStore,
  SessionStore,
  LotsStore,
  FilterStore,
} from './stores';
import Main from './pages/Main';
import { initPollyfils } from './core/utils';

initPollyfils();

const theme = createMuiTheme({
  palette: {
    primary: blue,
  },
});

const api = new API(process.env.REACT_APP_API_URL);
const errorStore = new ErrorStore(api);
const sessionStore = new SessionStore(errorStore, api);
const filterStore = new FilterStore(sessionStore);
const lotsStore = new LotsStore(filterStore, sessionStore, api);

const stores = {
  errorStore,
  sessionStore,
  filterStore,
  lotsStore,
};

sessionStore.init();

const App = () => (
  <Provider {...stores}>
    <MuiThemeProvider theme={theme}>
      <Main />
      {/* <DevTools /> */}
    </MuiThemeProvider>
  </Provider>
);

export default App;
