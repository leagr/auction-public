import jwtDecode from 'jwt-decode';
import WS from './WS';

const TOKEN_KEY = 'token';

const PROTOCOL_END = window.location.protocol === 'https:' ? 's' : '';

class API {
  constructor(host) {
    this.apiURL = `http${PROTOCOL_END}://${host}`;
    this.wsURL = `ws${PROTOCOL_END}://${host}`;

    this.ws = new WS();
  }

  getToken = () => localStorage.getItem(TOKEN_KEY);

  updateToken = token => localStorage.setItem(TOKEN_KEY, token);

  clearToken = () => localStorage.removeItem(TOKEN_KEY);

  openWS = () => {
    if (this.getToken()) {
      this.ws.open(`${this.wsURL}?token=${this.getToken()}`);

      this.ws.wsSubject.connectionObserver.next({ isConnected: true });
    }
  };

  get = async (url) => {
    const req = {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${this.getToken()}`,
        'Content-Type': 'application/json',
      },
    };

    return fetch(url, req);
  };

  signIn = async (credentials) => {
    const url = `${this.apiURL}/login`;

    const username = encodeURIComponent(credentials.username);
    const password = encodeURIComponent(credentials.password);

    const req = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: `username=${username}&password=${password}`,
    };

    const resp = await fetch(url, req);

    if (resp.status === 404 || resp.status === 400) {
      return 'Ошибка авторизации';
    }

    if (resp.status !== 200) {
      return null;
    }

    const { token } = await resp.json();

    this.updateToken(token);

    this.openWS();

    const { user } = jwtDecode(token);

    return user;
  };

  signOut = () => {
    this.ws.close();
    this.clearToken();
  };

  getUser = async () => {
    const token = this.getToken();
    if (!token) {
      return null;
    }

    const claims = jwtDecode(token);

    const url = `${this.apiURL}/users/${claims.user.id}`;

    const resp = await this.get(url);

    if (resp.status !== 200) {
      return null;
    }

    const user = await resp.json();

    this.openWS();

    return user;
  };
}

export default API;
