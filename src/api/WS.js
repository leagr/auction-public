import { share, filter, pluck } from 'rxjs/operators';
import jwtDecode from 'jwt-decode';

import WSSubject from './WSSubject';

const EDIT_USER = 'edit.user';
const GET_LOT = 'get.lot';
const GET_LOTS = 'get.lots';
const PLACE_BET = 'place.bet';
const CANCEL_BET = 'cancel.bet';
const CONFIRM_LOT = 'confirm.lot';
const COMPLETE_LOT = 'complete.lot';
const DELETE_CONFIRMATION = 'delete.confirmation';
const SEND_FEEDBACK = 'send.feedback';
const LOT_ADDED = 'event.lot.added';
const LOT_CHANGED = 'event.lot.changed';
const LOT_DELETED = 'event.lot.deleted';
const SYSTEM_INVALID_TOKEN = 'event.system.invalid.token';

const getFilters = userId => Object.keys(localStorage).reduce((acc, key) => {
  const prefix = `filter_${userId}_`;

  if (key && key.startsWith(prefix)) {
    return ({ ...acc, [key.substr(prefix.length)]: localStorage.getItem(key) });
  }

  return acc;
}, {});

const getToken = () => {
  const token = localStorage.getItem('token');

  if (token) {
    const { user: { id } } = jwtDecode(token);

    return id;
  }

  return 0;
};

class WS {
  constructor() {
    this.wsSubject = new WSSubject({
      reconnectAction: () => {
        const userId = getToken();

        if (userId) {
          this.getLots(getFilters(userId));
        }
      },
    });

    this.open = this.wsSubject.open;
    this.close = this.wsSubject.close;

    this.editUser = this.makeCommand(EDIT_USER);
    this.getLot = this.makeCommand(GET_LOT);
    this.getLots = this.makeCommand(GET_LOTS);
    this.placeBet = this.makeCommand(PLACE_BET);
    this.cancelBet = this.makeCommand(CANCEL_BET);
    this.confirmLot = this.makeCommand(CONFIRM_LOT);
    this.completeLot = this.makeCommand(COMPLETE_LOT);
    this.deleteConfirmation = this.makeCommand(DELETE_CONFIRMATION);
    this.sendFeedback = this.makeCommand(SEND_FEEDBACK);

    this.editUserSubscribe = this.makeResult(EDIT_USER);
    this.getLotSubscribe = this.makeResult(GET_LOT);
    this.getLotsSubscribe = this.makeResult(GET_LOTS);
    this.lotAddedSubscribe = this.makeEvent(LOT_ADDED);
    this.lotChangedSubscribe = this.makeEvent(LOT_CHANGED);
    this.lotDeletedSubscribe = this.makeEvent(LOT_DELETED);
    this.systemInvalidToken = this.makeEvent(SYSTEM_INVALID_TOKEN);

    this.errorSubscribe = this.catchFailedEvents;
  }

  makeCommand = type => (payload = null) => this.wsSubject.send({
    type: `command.${type}`,
    payload,
  });

  makeEvent = type => next => this.wsSubject.pipe(
    share(),
    filter(msg => msg.type === type),
    pluck('payload'),
  ).subscribe(next);

  makeResult = type => (success) => {
    this.makeEvent(`event.${type}.success`)(success);
  };

  catchFailedEvents = next => this.wsSubject.pipe(
    share(),
    filter(msg => msg.type.endsWith('.failed')),
  ).subscribe(next);
}

export default WS;
