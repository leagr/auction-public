import { Subject, Observable, interval, Subscriber } from 'rxjs';
import { share, distinctUntilChanged, takeWhile, tap } from 'rxjs/operators';
import { webSocket } from 'rxjs/webSocket';

class WSSubject extends Subject {
  constructor({
    reconnectInterval = 5000,
    reconnectAttempts = 60,
    reconnectAction,
  }) {
    super();

    this.currentAttempt = 0;
    this.reconnectInterval = reconnectInterval;
    this.reconnectAttempts = reconnectAttempts;
    this.reconnectAction = reconnectAction;
    this.setConnected = (isConnected, action) => {
      this.connectionObserver.next({ isConnected, action });
      this.isConnected = isConnected;
    };

    this.connectionStatus = new Observable((observer) => {
      this.connectionObserver = observer;
    }).pipe(
      share(),
      distinctUntilChanged((x, y) => x.isConnected === y.isConnected && x.action === y.action),
    );

    this.wsConfig = {
      closeObserver: {
        next: (e) => {
          if (!e.wasClean) {
            this.setConnected(false);
          }
        },
      },
      openObserver: {
        next: () => {
          this.setConnected(true);
        },
      },
    };

    this.connectionStatus.subscribe(({ isConnected }) => {
      if (!this.reconnectionObservable && !isConnected) {
        this.reconnect();
      }
    });

    this.reconnectSubscriberCreator();
  }

  reconnectSubscriberCreator = () => {
    this.reconnectSubscriber = new Subscriber(
      async () => {
        await this.connect();
        await this.reconnectAction();
      },
      null,
      () => {
        this.currentAttempt = 0;
        this.setConnected(true);
        if (!this.socket) {
          this.complete();
          this.connectionObserver.complete();
        }
      },
    );
  }

  connect = () => {
    this.reconnectionObservable = null;
    this.reconnectSubscriberCreator();
    this.socket = webSocket(this.wsConfig);
    this.socket.subscribe(
      msg => this.next(msg),
      () => {
        // console.log(e);
      },
    );
  };

  reconnect = () => {
    this.reconnectionObservable = interval(this.reconnectInterval)
      .pipe(
        takeWhile((_, i) => {
          this.currentAttempt = i;
          return i < this.reconnectAttempts && !this.isConnected;
        }),
        tap(() => {
          if (this.currentAttempt === this.reconnectAttempts - 1) {
            this.setConnected(false, 'close');
          }
        }),
      );

    this.reconnectionObservable.subscribe(this.reconnectSubscriber);
  };

  open = (url) => {
    this.wsConfig.url = url;
    this.connect();
  };

  close = () => {
    this.closeReconnectionObserver();
    this.socket.unsubscribe();
    this.socket = null;
  };

  closeReconnectionObserver = () => {
    this.reconnectSubscriber.unsubscribe();
    this.reconnectSubscriber = null;
    this.reconnectionObservable = null;
  }

  send = msg => this.socket.next(msg);
}

export default WSSubject;
