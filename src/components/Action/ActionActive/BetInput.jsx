import React from 'react';
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';
import {
  compose,
  withHandlers,
  withProps,
} from 'recompose';
import {
  RemoveCircleOutline,
  AddCircleOutline,
} from '@material-ui/icons';
import {
  Grid,
  IconButton,
  Typography,
} from '@material-ui/core';

const enhance = compose(
  withHandlers({
    handleMinus: ({ value, step, onChange }) => () => onChange(value - step),
    handlePlus: ({ value, step, onChange }) => () => onChange(value + step),
  }),
  withProps(({
    value, min, max, step, hasBet,
  }) => ({
    minusDisabled: value <= min + step,
    plusDisabled: hasBet ? value >= max - step : value >= max,
  })),
);

const BetInput = enhance(({
  value, minusDisabled, plusDisabled, handleMinus, handlePlus,
}) => (
  <Grid container alignItems="center">
    <Grid item>
      <IconButton disabled={minusDisabled} onClick={handleMinus}>
        <RemoveCircleOutline />
      </IconButton>
    </Grid>
    <Grid item xs>
      <Typography variant="subheading" align="center">
        <NumberFormat
          thousandSeparator
          displayType="text"
          prefix={'\u20bd'}
          value={value}
        />
      </Typography>
    </Grid>
    <Grid item>
      <IconButton disabled={plusDisabled} onClick={handlePlus}>
        <AddCircleOutline />
      </IconButton>
    </Grid>
  </Grid>
));

BetInput.propTypes = {
  value: PropTypes.number.isRequired,
  min: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
  step: PropTypes.number.isRequired,
  hasBet: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default BetInput;
