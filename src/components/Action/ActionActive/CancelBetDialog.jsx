import React from 'react';
import PropTypes from 'prop-types';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
} from '@material-ui/core';

const CancelBetDialog = ({ onClose, onSubmit, open }) => (
  <Dialog open={open} onClose={onClose}>
    <DialogTitle>Отменить ставку</DialogTitle>
    <DialogContent>
      <DialogContentText>
        Вы действительно хотите отменить сделанную вами ставку?
      </DialogContentText>
    </DialogContent>
    <DialogActions>
      <Button color="primary" onClick={onClose}>Нет</Button>
      <Button color="primary" onClick={onSubmit}>Да</Button>
    </DialogActions>
  </Dialog>
);

CancelBetDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};

export default CancelBetDialog;
