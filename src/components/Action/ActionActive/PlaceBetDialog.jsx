import React from 'react';
import PropTypes from 'prop-types';
import {
  compose,
  lifecycle,
  withProps,
  withHandlers,
  branch,
} from 'recompose';
import Yup from 'yup';
import NumberFormat from 'react-number-format';
import { withFormik } from 'formik';
import { withStyles } from '@material-ui/core/styles';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  TextField,
  Button,
} from '@material-ui/core';

import BetInput from './BetInput';

const styles = theme => ({
  form: {
    minWidth: 220,
    minHeight: 60,
  },
  dialogContent: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  a: {
    ...theme.typography.caption,
    textAlign: 'center',
    textDecoration: 'none',
    color: theme.palette.primary.main,
    alignSelf: 'flex-end',
  },
});

// eslint-disable-next-line react/prop-types
const NumberInput = ({ inputRef, onChange, ...rest }) => (
  <NumberFormat
    {...rest}
    thousandSeparator
    prefix={'\u20bd'}
    ref={inputRef}
    onValueChange={({ value }, e) => {
      e.target.value = Number(value);
      onChange(e);
    }}
  />
);

const enhance = compose(
  branch(
    ({ lot }) => lot.isEmptyCurrentPrice,
    withProps(() => ({ value: 0 })),
    branch(
      ({ lot }) => lot.hasBet,
      withProps(({ lot }) => ({ value: lot.currentPrice - lot.betStep })),
      withProps(({ lot }) => ({ value: lot.currentPrice })),
    ),
  ),
  withProps(({ value }) => ({ value })),
  withFormik({
    displayName: 'PlaceBetDialog',
    mapPropsToValues: ({ value }) => ({
      value,
    }),
    validationSchema: ({ lot: { betStep } }) => Yup.object().shape({
      value: Yup.number()
        .required('Введите ставку')
        .moreThan(betStep - 1, `Значение должно быть больше ${betStep}`),
    }),
    handleSubmit: (values, { props: { onSubmit } }) => {
      setTimeout(() => onSubmit({ value: Number(values.value) }), 1);
    },
  }),
  withHandlers({
    setBasePrice: ({ lot, setFieldValue }) => (e) => {
      e.preventDefault();
      setFieldValue('value', lot.basePrice);
    },
  }),
  lifecycle({
    componentDidUpdate(oldProps) {
      const { lot, setFieldValue } = this.props;

      if (lot.currentPrice !== oldProps.lot.currentPrice) {
        setFieldValue('value', lot.currentPrice - lot.betStep);
      }
    },
  }),
  withStyles(styles),
);

const PlaceBetDialog = enhance(({
  lot,
  onClose,
  values,
  errors,
  setBasePrice,
  setFieldValue,
  handleSubmit,
  handleChange,
  classes,
  open,
}) => (
  <Dialog open={open} onClose={onClose}>
    <DialogTitle>Сделать ставку</DialogTitle>
    <DialogContent className={classes.dialogContent}>
      {
        lot.isEmptyCurrentPrice &&
          <DialogContentText>Необходимо задать начальную ставку</DialogContentText>
      }
      <form
        className={classes.form}
        id="place_bet_dialog_form"
        onSubmit={handleSubmit}
      >
        {lot.isEmptyCurrentPrice && !lot.isPick ? (
          <TextField
            id="value"
            fullWidth
            autoFocus
            error={Boolean(errors.value)}
            helperText={errors.value}
            value={values.value}
            onChange={handleChange}
            InputProps={{
              inputComponent: NumberInput,
            }}
          />
        ) : (
          <BetInput
            value={values.value}
            min={0}
            max={lot.currentPrice}
            step={lot.betStep}
            hasBet={lot.hasBet}
            onChange={value => setFieldValue('value', value)}
          />
        )}
      </form>
      {
        (!lot.isPick) && (
          // eslint-disable-next-line jsx-a11y/anchor-is-valid
          <a
            className={classes.a}
            role="button"
            href=""
            onClick={setBasePrice}
          >
            Установить базовую цену
          </a>
        )
      }
    </DialogContent>
    <DialogActions>
      <Button color="primary" onClick={onClose}>Отменить</Button>
      <Button
        type="submit"
        form="place_bet_dialog_form"
        color="primary"
      >
        Отправить
      </Button>
    </DialogActions>
  </Dialog>
));

PlaceBetDialog.propTypes = {
  lot: PropTypes.shape({
    isEmptyCurrentPrice: PropTypes.bool.isRequired,
    betStep: PropTypes.number.isRequired,
    currentPrice: PropTypes.number,
  }).isRequired,
  onClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};

export default PlaceBetDialog;
