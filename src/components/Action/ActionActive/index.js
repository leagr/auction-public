import PropTypes from 'prop-types';
import {
  branch,
  compose,
  setPropTypes,
  defaultProps,
  withHandlers,
  renderNothing,
  renderComponent,
} from 'recompose';

import ButtonBase from '../ButtonBase';
import MenuItemBase from '../MenuItemBase';
import PlaceBetDialog from './PlaceBetDialog';
import CancelBetDialog from './CancelBetDialog';

export default compose(
  setPropTypes({
    lot: PropTypes.shape({
      isBet: PropTypes.bool.isRequired,
      isHot: PropTypes.bool.isRequired,
    }),
  }),
  branch(
    ({ lot }) => lot.isBet,
    branch(
      ({ lot }) => lot.isHot,
      renderNothing,
      compose(
        setPropTypes({
          lot: PropTypes.shape({
            cancelBet: PropTypes.func.isRequired,
          }),
        }),
        defaultProps({
          label: 'Отменить ставку',
          color: 'secondary',
          Dialog: CancelBetDialog,
        }),
        withHandlers({
          onAction: ({ lot }) => () => {
            lot.cancelBet();
          },
        }),
      ),
    ),
    compose(
      setPropTypes({
        lot: PropTypes.shape({
          placeBet: PropTypes.func.isRequired,
        }),
      }),
      defaultProps({
        label: 'Сделать ставку',
        color: 'primary',
        Dialog: PlaceBetDialog,
      }),
      withHandlers({
        onAction: ({ lot }) => ({ value }) => {
          lot.placeBet(value);
        },
      }),
    ),
  ),
  branch(
    ({ type }) => type === 'button',
    renderComponent(ButtonBase),
  ),
)(MenuItemBase);
