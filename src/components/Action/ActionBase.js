import compose from 'recompose/compose';
import PropTypes from 'prop-types';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';
import setPropTypes from 'recompose/setPropTypes';

const ActionBase = compose(
  setPropTypes({
    onOpen: PropTypes.func,
    onAction: PropTypes.func.isRequired,
    onClose: PropTypes.func,
  }),
  withState('open', 'setOpen', false),
  withHandlers({
    handleOpen: ({ setOpen, onOpen }) => async () => {
      if (onOpen) {
        await onOpen();
      }

      setOpen(true);
    },
    handleClose: ({ setOpen, onClose }) => () => {
      setOpen(false);

      if (onClose) onClose();
    },
    handleSubmit: ({ setOpen, onAction, onClose }) => async (values) => {
      await onAction(values);

      setOpen(false);

      if (onClose) onClose();
    },
  }),
);

export default ActionBase;
