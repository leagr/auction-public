import React from 'react';
import PropTypes from 'prop-types';

import DeleteConfirmationMenuItem from './DeleteConfirmationMenuItem';
import ConfirmLotMenuItem from './ConfirmLotMenuItem';

const Actions = ({
  objectType,
  lot,
  getLot,
  isLoading,
  lotIsLoading,
  isConnected,
  onClose,
}) => (
  <React.Fragment>
    <ConfirmLotMenuItem
      lotIsLoading={lotIsLoading}
      objectType={objectType}
      lot={lot}
      getLot={getLot}
      isLoading={isLoading}
      isConnected={isConnected}
      onClose={onClose}
    />
    <DeleteConfirmationMenuItem
      lot={lot}
      isConnected={isConnected}
      onClose={onClose}
    />
  </React.Fragment>
);

Actions.propTypes = {
  objectType: PropTypes.string.isRequired,
  lot: PropTypes.objectOf(PropTypes.any).isRequired,
  getLot: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  lotIsLoading: PropTypes.bool.isRequired,
  isConnected: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default Actions;
