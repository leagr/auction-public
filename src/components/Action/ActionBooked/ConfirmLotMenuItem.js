import withProps from 'recompose/withProps';

import MenuItemBase from '../MenuItemBase';
import { Dialogs as ObjectDialogs } from '../../Object';

const LotConfirmButton = withProps(({
  lot,
  getLot,
  objectType,
  lotIsLoading,
}) => ({
  onOpen: () => {
    getLot(lot.id);
  },
  onAction: lot.confirmLot,
  Dialog: ObjectDialogs.ConfirmLotDialog(objectType),
  label: lot.isConfirmed ? 'Редактировать' : 'Подтвердить',
  color: 'primary',
  DialogProps: {
    lotIsLoading,
    confirm: lot.confirm,
    title: lot.isConfirmed ? 'Редактирование' : 'Подтверждение',
  },
}))(MenuItemBase);

export default LotConfirmButton;
