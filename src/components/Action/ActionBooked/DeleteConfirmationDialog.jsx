import React from 'react';
import PropTypes from 'prop-types';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
} from '@material-ui/core';

const DeleteConfirmationDialog = ({ onClose, onSubmit, open }) => (
  <Dialog open={open} onClose={onClose}>
    <DialogTitle>Отменить бронирование</DialogTitle>
    <DialogContent>
      <DialogContentText>
        Вы действительно хотите отменить бронирование?
      </DialogContentText>
    </DialogContent>
    <DialogActions>
      <Button color="primary" onClick={onClose}>Закрыть</Button>
      <Button color="primary" onClick={onSubmit}>Отменить бронирование</Button>
    </DialogActions>
  </Dialog>
);

DeleteConfirmationDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};

export default DeleteConfirmationDialog;

