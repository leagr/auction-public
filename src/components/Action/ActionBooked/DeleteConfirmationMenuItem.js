import { compose, withProps, defaultProps } from 'recompose';

import MenuItemBase from '../MenuItemBase';
import DeleteConfirmationDialog from './DeleteConfirmationDialog';

const DeleteConfirmationMenuItem = compose(
  withProps(({ lot }) => ({
    onAction: lot.isConfirmed ?
      lot.deleteConfirmation :
      lot.cancelBet,
  })),
  defaultProps({
    label: 'Отменить',
    color: 'secondary',
    Dialog: DeleteConfirmationDialog,
  }),
)(MenuItemBase);

export default DeleteConfirmationMenuItem;
