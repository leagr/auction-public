import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import compose from 'recompose/compose';
import renderComponent from 'recompose/renderComponent';
import renderNothing from 'recompose/renderNothing';
import branch from 'recompose/branch';
import { observer } from 'mobx-react';
import Yup from 'yup';
import { withFormik } from 'formik';
import { withStyles } from '@material-ui/core/styles';
import {
  Dialog,
  DialogContent,
  DialogActions,
  DialogTitle,
  Button,
} from '@material-ui/core';

import { CLIENT_DATE_TIME_FORMAT } from '../../../../constants/date';
import Form from './Form';
import List from './List';

const styles = {
  form: {
    minWidth: 400,
  },
  textField: {
    flex: 1,
    padding: '8px 0',
  },
  listItem: {
    minHeight: 20,
  },
  dialogPreload: {
    opacity: 1,
  },
};

const Content = branch(
  ({ hasComplete }) => hasComplete,
  renderComponent(List),
)(Form);

const enhance = branch(
  ({ lotIsLoading }) => !lotIsLoading,
  compose(
    withFormik({
      displayName: 'ActDialog',
      mapPropsToValues: ({
        complete,
      }) => complete || ({
        act_number: '',
        act_date: '',
        send_date: '',
        receive_date: '',
        receive_repeat_date: '',
        registry_date: '',
        payment_date: '',
        rise_in_price: '',
        comment: '',
      }),
      validationSchema: Yup.object().shape({
        act_number: Yup.number()
          .typeError('некорректное значение')
          .required('Введите номер акта'),
        act_date: Yup.string()
          .test(
            'act_date',
            'Некорректное значение',
            value => moment(value, CLIENT_DATE_TIME_FORMAT).isValid(),
          )
          .min(8, 'Некорректное значение')
          .required('Введите дату акта'),
        send_date: Yup.string()
          .test(
            'send_date',
            'Некорректное значение',
            value => moment(value, CLIENT_DATE_TIME_FORMAT).isValid(),
          )
          .min(8, 'Некорректное значение')
          .required('Введите дату отправки акта'),
      }),
      handleSubmit: async (values, { props: { onSubmit, lot } }) => {
        const {
          act_date: actDate,
          send_date: sendDate,
          act_number: actNumber,
        } = values;

        const info = {
          act_number: actNumber,
          act_date: moment(actDate, CLIENT_DATE_TIME_FORMAT).utcOffset(0, true).format(),
          send_date: moment(sendDate, CLIENT_DATE_TIME_FORMAT).utcOffset(0, true).format(),
        };

        await onSubmit({
          lot_id: lot.id,
          info,
        });

        await lot.setComplete(info);
      },
    }),
    withStyles(styles),
    observer,
  ),
  renderNothing,
);

const ActDialog = enhance(({
  onClose,
  handleChange,
  values,
  errors,
  touched,
  handleSubmit,
  classes,
  hasComplete,
  open,
}) => (
  <Dialog open={open} onClose={onClose}>
    <DialogTitle>Акт</DialogTitle>
    <DialogContent>
      <form
        className={classes.form}
        id="place_bet_dialog_form"
        onSubmit={handleSubmit}
      >
        <Content
          hasComplete={hasComplete}
          classes={classes}
          errors={errors}
          values={values}
          touched={touched}
          handleChange={handleChange}
        />
      </form>
    </DialogContent>
    <DialogActions>
      <Button color="primary" onClick={onClose}>
        {hasComplete ? 'Закрыть' : 'Отменить'}
      </Button>
      {
        !hasComplete && (
          <Button
            type="submit"
            form="place_bet_dialog_form"
            color="primary"
          >
            Отправить
          </Button>
        )
      }
    </DialogActions>
  </Dialog>
));

ActDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  hasComplete: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool.isRequired,
  open: PropTypes.bool.isRequired,
};

export default ActDialog;
