import { withProps } from 'recompose';

import MenuItemBase from '../../MenuItemBase';
import ActDialog from './ActDialog';

const LotConfirmButton = withProps(({
  lot,
  getLot,
  lotIsLoading,
}) => ({
  onOpen: () => {
    getLot(lot.id);
  },
  onAction: lot.completeLot,
  Dialog: ActDialog,
  label: 'Акт',
  color: 'primary',
  DialogProps: {
    lotIsLoading,
    complete: lot.complete,
    hasComplete: lot.hasComplete,
    isCompleted: lot.isCompleted,
  },
}))(MenuItemBase);

export default LotConfirmButton;
