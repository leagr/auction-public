import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';

import DateTimeInput from '../../../DateTimeInput/index';

const Form = ({
  classes,
  touched,
  values,
  handleChange,
  errors,
}) => (
  <Grid container direction="column" spacing={16} justify="center">
    <Grid item>
      <TextField
        className={classes.textField}
        id="act_number"
        label="Номер акта"
        fullWidth
        error={Boolean(touched.act_number && errors.act_number)}
        helperText={touched.act_number && errors.act_number}
        value={values.act_number}
        onChange={handleChange}
      />
    </Grid>
    <Grid item>
      <TextField
        className={classes.textField}
        id="act_date"
        label="Дата акта"
        fullWidth
        error={Boolean(touched.act_date && errors.act_date)}
        helperText={touched.act_date && errors.act_date}
        value={values.act_date}
        onChange={handleChange}
        // eslint-disable-next-line react/jsx-no-duplicate-props
        InputProps={{
          inputComponent: DateTimeInput,
        }}
      />
    </Grid>
    <Grid item>
      <TextField
        className={classes.textField}
        id="send_date"
        label="Дата отправки акта"
        fullWidth
        error={Boolean(touched.send_date && errors.send_date)}
        helperText={touched.send_date && errors.send_date}
        value={values.send_date}
        onChange={handleChange}
        // eslint-disable-next-line react/jsx-no-duplicate-props
        InputProps={{
          inputComponent: DateTimeInput,
        }}
      />
    </Grid>
  </Grid>
);

Form.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  touched: PropTypes.objectOf(PropTypes.bool),
  values: PropTypes.objectOf(PropTypes.string),
  errors: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.bool])),
  handleChange: PropTypes.func.isRequired,
};

Form.defaultProps = {
  touched: {},
  values: {},
  errors: {},
};

export default Form;
