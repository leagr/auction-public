import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import BaseList from '../../../List';
import { SERVER_DATE_TIME_FORMAT } from '../../../../constants/date';

const formatTime = (value) => {
  if (value) {
    return moment.utc(value, SERVER_DATE_TIME_FORMAT).format('LLL');
  }

  return undefined;
};

const ACT_LABELS = {
  act_number: 'Номер акта',
  act_date: 'Дата акта',
  send_date: 'Дата отправки акта',
  receive_date: 'Дата получения акта',
  receive_repeat_date: 'Дата повторного получения акта',
  registry_date: 'Дата постановки в реестр',
  payment_date: 'Дата постановки в оплату',
  rise_in_price: 'Удорожание',
  comment: 'Комментарий',
};

const List = ({ values }) => {
  const normalizedValues = {
    ...values,
    act_date: formatTime(values.act_date),
    send_date: formatTime(values.send_date),
    receive_date: formatTime(values.receive_date),
    receive_repeat_date: formatTime(values.receive_repeat_date),
    payment_date: formatTime(values.payment_date),
    registry_date: formatTime(values.registry_date),
  };

  return (
    <BaseList
      showEmpty
      rows={ACT_LABELS}
      data={normalizedValues}
    />
  );
};

List.propTypes = {
  values: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ])).isRequired,
};

export default List;
