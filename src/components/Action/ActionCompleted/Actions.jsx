import React from 'react';
import PropTypes from 'prop-types';

import Act from './Act';
import ConfirmInfoMenuItem from './ConfirmInfoMenuItem';

const Actions = ({
  objectType,
  lot,
  getLot,
  isLoading,
  lotIsLoading,
  isConnected,
  onClose,
}) => (
  <React.Fragment>
    <Act
      lot={lot}
      isLoading={isLoading}
      isConnected={isConnected}
      getLot={getLot}
      lotIsLoading={lotIsLoading}
      onClose={onClose}
    />
    <ConfirmInfoMenuItem
      lot={lot}
      objectType={objectType}
      isLoading={isLoading}
      isConnected={isConnected}
      getLot={getLot}
      onClose={onClose}
    />
  </React.Fragment>
);

Actions.propTypes = {
  objectType: PropTypes.string.isRequired,
  lot: PropTypes.objectOf(PropTypes.any).isRequired,
  getLot: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  lotIsLoading: PropTypes.bool.isRequired,
  isConnected: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default Actions;

