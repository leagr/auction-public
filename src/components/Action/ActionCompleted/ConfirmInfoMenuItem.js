import withProps from 'recompose/withProps';

import MenuItemBase from '../MenuItemBase';
import { Dialogs as ObjectDialogs } from '../../Object';

const ConfirmInfoMenuItem = withProps(({
  lot,
  getLot,
  objectType,
}) => ({
  onOpen: () => {
    getLot(lot.id);
  },
  onAction: () => {},
  Dialog: ObjectDialogs.ConfirmInfoDialog(objectType),
  label: 'Информация',
  DialogProps: {
    confirm: lot.confirm,
    title: 'Информация',
  },
}))(MenuItemBase);

export default ConfirmInfoMenuItem;
