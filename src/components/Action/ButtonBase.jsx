import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';

import ActionBase from './ActionBase';

const ButtonBase = ActionBase(({
  handleOpen,
  handleSubmit,
  handleClose,
  lot,
  label,
  color,
  isConnected,
  isLoading,
  DialogProps,
  Dialog,
  open,
}) => (
  <React.Fragment>
    <Button
      fullWidth
      size="small"
      variant="outlined"
      color={color}
      onClick={handleOpen}
      disabled={!isConnected}
    >
      {label}
    </Button>
    <Dialog
      open={open}
      lot={lot}
      onClose={handleClose}
      onSubmit={handleSubmit}
      isLoading={isLoading}
      {...DialogProps}
    />
  </React.Fragment>
));

ButtonBase.propTypes = {
  label: PropTypes.string.isRequired,
  color: PropTypes.string,
  Dialog: PropTypes.func.isRequired,
  DialogProps: PropTypes.objectOf(PropTypes.any),
  lot: PropTypes.objectOf(PropTypes.any).isRequired,
  isConnected: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool,
};

ButtonBase.defaultProps = {
  isLoading: false,
};

ButtonBase.defaultProps = {
  color: 'primary',
  DialogProps: {},
};

export default ButtonBase;
