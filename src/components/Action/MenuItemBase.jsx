import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import ActionBase from './ActionBase';

const styles = theme => ({
  primary: {
    color: theme.palette.primary.main,
  },
  secondary: {
    color: theme.palette.secondary.main,
  },
});

const MenuItemBase = ActionBase(({
  handleOpen,
  handleSubmit,
  handleClose,
  lot,
  label,
  color,
  isLoading,
  DialogProps,
  Dialog,
  open,
  classes,
}) => (
  <React.Fragment>
    <MenuItem onClick={handleOpen}>
      <Typography
        variant="button"
        className={classNames({
          [classes.primary]: color === 'primary',
          [classes.secondary]: color === 'secondary',
        })}
      >
        {label}
      </Typography>
    </MenuItem>
    <Dialog
      open={open}
      lot={lot}
      onClose={handleClose}
      onSubmit={handleSubmit}
      isLoading={isLoading}
      {...DialogProps}
    />
  </React.Fragment>
));

MenuItemBase.propTypes = {
  label: PropTypes.string.isRequired,
  color: PropTypes.string,
  Dialog: PropTypes.func.isRequired,
  DialogProps: PropTypes.objectOf(PropTypes.any),
  lot: PropTypes.objectOf(PropTypes.any).isRequired,
  isConnected: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool,
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
};

MenuItemBase.defaultProps = {
  isLoading: false,
};

MenuItemBase.defaultProps = {
  color: 'default',
  DialogProps: {},
};

export default withStyles(styles)(MenuItemBase);
