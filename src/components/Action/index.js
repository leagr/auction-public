import { PropTypes } from 'prop-types';
import {
  compose,
  setPropTypes,
  withProps,
  componentFromProp,
} from 'recompose';
import * as states from '../../constants/states';
import ActionActive from './ActionActive';
import ActionBooked from './ActionBooked';
import ActionCompleted from './ActionCompleted';

const component = {
  [states.ACTIVE]: ActionActive,
  [states.BOOKED]: ActionBooked,
  [states.COMPLETED]: ActionCompleted,
};

export default compose(
  setPropTypes({
    state: PropTypes.string.isRequired,
  }),
  withProps(({
    state,
  }) => ({
    component: component[state],
  })),
)(componentFromProp('component'));
