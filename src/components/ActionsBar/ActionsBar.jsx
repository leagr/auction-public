import React from 'react';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';
import SpeedDial from '@material-ui/lab/SpeedDial';
import SpeedDialAction from '@material-ui/lab/SpeedDialAction';
import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon';
import withStyles from '@material-ui/core/styles/withStyles';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import FeedbackIcon from '@material-ui/icons/Feedback';
import PriorityHighIcon from '@material-ui/icons/PriorityHigh';

import HelpDialog from '../Help';
import FeedbackDialog from '../FeedbackDialog';

const styles = {
  speedDial: {
    zIndex: 10,
    position: 'fixed',
    left: 'calc(100vw - 60px)',
    top: 'calc(100vh - 190px)',
  },
};

const ActionsBar = ({
  username,
  classes,
  sendFeedback,
  open,
  handleClose,
  handleClick,
  openedKey,
  setOpenedKey,
}) => {
  const renderDialog = () => {
    switch (openedKey) {
      case 'help': {
        return <HelpDialog handleClose={() => setOpenedKey('')} />;
      }
      case 'feedback': {
        return (
          <FeedbackDialog
            username={username}
            sendFeedback={sendFeedback}
            handleClose={() => setOpenedKey('')}
          />
        );
      }
      default: {
        return null;
      }
    }
  };

  return (
    <React.Fragment>
      <SpeedDial
        className={classes.speedDial}
        icon={
          <SpeedDialIcon
            icon={<PriorityHighIcon />}
            openIcon={<PriorityHighIcon />}
          />
        }
        ariaLabel=""
        open={open}
        onClose={handleClose}
        onMouseLeave={handleClose}
        onClick={handleClick}
        direction="up"
      >
        <SpeedDialAction
          icon={<HelpOutlineIcon />}
          tooltipTitle="Руководство"
          onClick={() => setOpenedKey('help')}
        />
        <SpeedDialAction
          icon={<FeedbackIcon />}
          tooltipTitle="Задать вопрос"
          onClick={() => setOpenedKey('feedback')}
        />
      </SpeedDial>
      {renderDialog()}
    </React.Fragment>
  );
};

ActionsBar.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  open: PropTypes.bool.isRequired,
  username: PropTypes.string.isRequired,
  sendFeedback: PropTypes.func.isRequired,
  handleClose: PropTypes.func.isRequired,
  handleClick: PropTypes.func.isRequired,
  openedKey: PropTypes.string.isRequired,
  setOpenedKey: PropTypes.func.isRequired,
};

const enhance = compose(
  withStyles(styles),
  withState('open', 'setOpen', false),
  withState('openedKey', 'setOpenedKey', ''),
  withHandlers({
    handleClick: ({ setOpen, open }) => () => setOpen(!open),
    handleOpen: ({ setOpen }) => () => setOpen(true),
    handleClose: ({ setOpen }) => () => setOpen(false),
  }),
);

export default enhance(ActionsBar);
