import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import HomeIcon from '@material-ui/icons/Home';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';

import HeaderMenu from './AppHeaderMenu';

const styles = theme => ({
  title: {
    flex: 1,
    marginLeft: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 2,
    cursor: 'default',
  },
  username: {
    marginRight: theme.spacing.unit * 2,
  },
});

const AppHeader = ({
  classes, onHome, onProfile, onSignOut, username,
}) => (
  <AppBar position="fixed">
    <Toolbar>
      <IconButton color="inherit" onClick={onHome}>
        <HomeIcon />
      </IconButton>
      <Typography className={classes.title} color="inherit" variant="title">
        Аукцион NEFIS
      </Typography>
      <Typography className={classes.username} color="inherit" variant="title">
        {username}
      </Typography>
      <HeaderMenu onProfile={onProfile} onSignOut={onSignOut} />
    </Toolbar>
  </AppBar>
);

AppHeader.propTypes = {
  onHome: PropTypes.func.isRequired,
  onProfile: PropTypes.func.isRequired,
  onSignOut: PropTypes.func.isRequired,
  username: PropTypes.string.isRequired,
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
};

export default withStyles(styles)(AppHeader);
