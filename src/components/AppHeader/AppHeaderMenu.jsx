import React from 'react';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import setPropTypes from 'recompose/setPropTypes';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const AppHeaderMenu = ({
  // eslint-disable-next-line react/prop-types
  anchor, handleClick, handleClose, handleProfile, handleSignOut,
}) => (
  <div>
    <IconButton color="inherit" onClick={handleClick}>
      <AccountCircleIcon />
    </IconButton>
    <Menu anchorEl={anchor} open={Boolean(anchor)} onClose={handleClose}>
      <MenuItem onClick={handleProfile}>Профиль</MenuItem>
      <MenuItem onClick={handleSignOut}>Выход</MenuItem>
    </Menu>
  </div>
);

export default compose(
  setPropTypes({
    onProfile: PropTypes.func.isRequired,
    onSignOut: PropTypes.func.isRequired,
  }),
  withState('anchor', 'setAnchor', null),
  withHandlers({
    handleClick: ({ setAnchor }) => event => setAnchor(event.currentTarget),
    handleClose: ({ setAnchor }) => () => setAnchor(null),
    handleProfile: ({ setAnchor, onProfile }) => () => {
      setAnchor(null);
      onProfile();
    },
    handleSignOut: ({ setAnchor, onSignOut }) => () => {
      setAnchor(null);
      onSignOut();
    },
  }),
)(AppHeaderMenu);
