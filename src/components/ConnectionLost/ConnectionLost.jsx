import React from 'react';
import PropTypes from 'prop-types';
import Snackbar from '@material-ui/core/Snackbar';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  snackbar: {
    marginBottom: theme.spacing.unit * 5,
  },
});

const ConnectionLost = ({ isConnected, classes, text }) => (
  <Snackbar
    className={classes.snackbar}
    open={!isConnected}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    message={
      <Typography color="inherit">
        {text}
      </Typography>
    }
  />
);

ConnectionLost.propTypes = {
  isConnected: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired,
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
};

export default withStyles(styles)(ConnectionLost);
