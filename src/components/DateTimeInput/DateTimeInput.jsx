import React from 'react';
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';

const DATE_MASK = ['Д', 'Д', 'М', 'М', 'Г', 'Г', 'Г', 'Г'];
const TIME_MASK = ['ч', 'ч', 'м', 'м'];
const DATE_FORMAT = '##/##/####';
const TIME_FORMAT = '##:##';

const DateTimeInput = ({
  inputRef,
  onChange,
  time,
  ...rest
}) => {
  let mask = DATE_MASK;
  let format = DATE_FORMAT;

  if (time) {
    mask = [...DATE_MASK, ...TIME_MASK];
    format = `${DATE_FORMAT} ${TIME_FORMAT}`;
  }

  return (
    <NumberFormat
      {...rest}
      format={format}
      mask={mask}
      onValueChange={({ value }, e) => {
        e.target.value = value;
        onChange(e);
      }}
      ref={inputRef}
    />
  );
};

DateTimeInput.propTypes = {
  inputRef: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  time: PropTypes.bool,
};

DateTimeInput.defaultProps = {
  time: false,
};

export default DateTimeInput;
