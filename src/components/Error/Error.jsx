import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { compose } from 'recompose';
import {
  IconButton,
  Snackbar,
  Typography,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import CloseIcon from '@material-ui/icons/Close';

const styles = theme => ({
  close: {
    width: theme.spacing.unit * 4,
    height: theme.spacing.unit * 4,
  },
});

const Error = ({
  classes,
  error,
  handleClose,
  show,
}) => (
  <Snackbar
    action={[
      <IconButton
        key="close"
        aria-label="Close"
        color="inherit"
        className={classes.close}
        onClick={handleClose}
      >
        <CloseIcon />
      </IconButton>,
    ]}
    anchorOrigin={{
      vertical: 'top',
      horizontal: 'right',
    }}
    autoHideDuration={30000}
    message={<Typography color="inherit">{error.error}</Typography>}
    onClose={handleClose}
    open={show}
  />
);

Error.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  error: PropTypes.shape({
    error: PropTypes.string,
    type: PropTypes.string,
  }).isRequired,
  handleClose: PropTypes.func.isRequired,
  show: PropTypes.bool.isRequired,
};

Error.defaultProps = {
  error: {
    error: '',
    type: '',
  },
};

export default compose(
  withStyles(styles),
  observer,
)(Error);
