import React from 'react';
import PropTypes from 'prop-types';
import Yup from 'yup';
import compose from 'recompose/compose';
import { withFormik } from 'formik';
import {
  Dialog,
  DialogContent,
  DialogTitle,
  IconButton,
  Button,
  TextField,
  Grid,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';

const styles = theme => ({
  dialogTitle: {
    paddingTop: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
  },
  dialogContent: {
    paddingBottom: theme.spacing.unit * 2,
  },
});

const enhance = compose(
  withFormik({
    displayName: 'FeedbackForm',
    mapPropsToValues: () => ({
      name: '',
      phone_number: '',
      email: '',
      message: '',
    }),
    validationSchema: Yup.object().shape({
      name: Yup.string().required('Введите имя'),
      phone_number: Yup.string().required('Введите номер телефона'),
      email: Yup.string().required('Введите E-mail'),
      message: Yup.string().required('Введите текст сообщения'),
    }),
    handleSubmit: (values, { props: { sendFeedback, handleClose, username } }) => {
      const message = `## Обращение от ТЭК (${username})\n\n` +
        `**Имя:** ${values.name}\n\n` +
        `**Номер телефона:** ${values.phone_number}\n\n` +
        `**E-mail:** ${values.email}\n\n` +
        `**Сообщение:**\n\n${values.message}`;

      sendFeedback(message);

      handleClose();
    },
  }),
  withStyles(styles),
);

const FeedbackDialog = enhance(({
  /* eslint-disable react/prop-types */
  values,
  errors,
  touched,
  handleChange,
  handleSubmit,
  classes,
  /* eslint-enable react/prop-types */
  handleClose,
}) => {
  // eslint-disable-next-line react/prop-types
  const renderField = ({ value, ...rest }) => (
    <TextField
      id={value}
      error={Boolean(touched[value] && errors[value])}
      helperText={touched[value] && errors[value]}
      value={values[value] || ''}
      {...rest}
    />
  );

  return (
    <Dialog
      onClose={handleClose}
      open
      maxWidth="sm"
      fullWidth
    >
      <DialogTitle className={classes.dialogTitle}>
        <Grid
          container
          direction="row"
          justify="space-between"
          alignItems="center"
        >
          Задать вопрос
          <IconButton onClick={handleClose}>
            <CloseIcon />
          </IconButton>
        </Grid>
      </DialogTitle>
      <DialogContent className={classes.dialogContent}>
        <Grid container direction="column" spacing={24}>
          <Grid item>
            {
              renderField({
                value: 'name',
                label: 'Имя',
                fullWidth: true,
                onChange: handleChange,
              })
            }
          </Grid>
          <Grid item>
            {
              renderField({
                value: 'phone_number',
                label: 'Номер телефона',
                fullWidth: true,
                onChange: handleChange,
              })
            }
          </Grid>
          <Grid item>
            {
              renderField({
                value: 'email',
                label: 'E-mail',
                fullWidth: true,
                onChange: handleChange,
              })
            }
          </Grid>
          <Grid item>
            {
              renderField({
                value: 'message',
                label: 'Сообщение',
                fullWidth: true,
                onChange: handleChange,
                multiline: true,
                rowsMax: 8,
                rows: 4,
              })
            }
          </Grid>
          <Grid item container direction="row" justify="center">
            <Button color="primary" onClick={handleSubmit}>
              Отправить
            </Button>
          </Grid>
        </Grid>
      </DialogContent>
    </Dialog>
  );
});

FeedbackDialog.propTypes = {
  username: PropTypes.string.isRequired,
  handleClose: PropTypes.func.isRequired,
};

export default FeedbackDialog;
