import React from 'react';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import lifecycle from 'recompose/lifecycle';
import withProps from 'recompose/withProps';
import { observer } from 'mobx-react';
import { withFormik } from 'formik';
import Yup from 'yup';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import NumberFormat from 'react-number-format';

import { Filter as ObjectFilter } from '../Object';

const styles = {
  filterItem: {
    minHeight: 68,
  },
};

const NumberInput = ({ inputRef, onChange, ...rest }) => (
  <NumberFormat
    {...rest}
    onValueChange={({ value }, e) => {
      e.target.value = value;
      onChange(e);
    }}
    ref={inputRef}
  />
);

const enhance = compose(
  withFormik({
    displayName: 'Fields',
    handleSubmit: (values, { props: { onSubmit, objectType } }) => {
      const normalizedValues = {
        lot_id: values.lot_id ? Number(values.lot_id) : undefined,
        end_price: values.end_price ? Number(values.end_price) : undefined,
        start_price: values.start_priceNumber ? (values.start_price) : undefined,
      };

      onSubmit({
        ...ObjectFilter.normalizer(objectType)(values),
        ...normalizedValues,
      });
    },
    validationSchema: ({ objectType }) => Yup.object().shape({
      ...ObjectFilter.validationSchema(objectType),
    }),
    mapPropsToValues: ({ filters, objectType }) =>
      ObjectFilter.mapPropsToValues(objectType)(filters),
  }),
  withProps(({ onClear, handleReset, values }) =>
    ({
      onReset: async () => { await onClear(); handleReset(); },
      hasValues: Object.keys(values).length > 0,
    })),
  lifecycle({
    componentDidUpdate(oldProps) {
      const { filters, onReset } = this.props;

      if (Object.keys(filters).length === 0 && oldProps.filters
        && Object.keys(oldProps.filters).length !== 0) {
        onReset();
      }
    },
  }),
  withStyles(styles),
  observer,
);

const Filter = enhance(({
  // eslint-disable-next-line react/prop-types
  values = {}, handleChange, handleSubmit, errors,
  classes,
  onReset,
  objectType,
  hasValues,
  state,
}) => (
  <form onSubmit={handleSubmit}>
    <ExpansionPanel defaultExpanded={hasValues}>
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <Typography variant="title" color="textSecondary">Поиск</Typography>
      </ExpansionPanelSummary>
      <div className={classes.form}>
        <ExpansionPanelDetails>
          <Grid container spacing={16}>
            <Grid item xs={4} className={classes.filterItem}>
              <TextField
                id="lot_id"
                fullWidth
                placeholder="№ лота"
                margin="none"
                onChange={handleChange}
                value={values.lot_id || ''}
                InputProps={{
                  inputComponent: NumberInput,
                }}
              />
            </Grid>
            <ObjectFilter.Fields
              objectType={objectType}
              values={values}
              errors={errors}
              handleChange={handleChange}
              state={state}
              classes={classes}
            />
            <Grid item xs={2} className={classes.filterItem}>
              <TextField
                id="start_price"
                fullWidth
                helperText="Базовая стоимость от"
                margin="none"
                onChange={handleChange}
                value={values.start_price || ''}
              />
            </Grid>
            <Grid item xs={2} className={classes.filterItem}>
              <TextField
                id="end_price"
                fullWidth
                helperText="до"
                margin="none"
                onChange={handleChange}
                value={values.end_price || ''}
              />
            </Grid>
          </Grid>
        </ExpansionPanelDetails>
        <Divider />
        <ExpansionPanelActions>
          <Button size="small" onClick={onReset} disabled={!hasValues}>
            Сбросить
          </Button>
          <Button type="submit" size="small" color="primary" disabled={!hasValues}>
            Применить
          </Button>
        </ExpansionPanelActions>
      </div>
    </ExpansionPanel>
  </form>
));

Filter.propTypes = {
  objectType: PropTypes.string.isRequired,
  state: PropTypes.string.isRequired,
};

export default Filter;
