import React from 'react';
import PropTypes from 'prop-types';
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Button,
  Typography,
} from '@material-ui/core';

import GuidLine from './guideline.mdx';

const HelpDialog = ({ handleClose }) => (
  <Dialog open fullWidth maxWidth="md" onClose={handleClose}>
    <DialogTitle>Руководство</DialogTitle>
    <DialogContent>
      <GuidLine
        components={{
          p: props => <Typography {...props} variant="body1" />,
          li: props => <Typography {...props} variant="body1" />,
          h3: props => <Typography {...props} variant="title" />,
          h4: props => <Typography {...props} variant="subheading" />,
        }}
      />
    </DialogContent>
    <DialogActions>
      <Button color="primary" onClick={handleClose}>
        Закрыть
      </Button>
    </DialogActions>
  </Dialog>
);

HelpDialog.propTypes = {
  handleClose: PropTypes.func.isRequired,
};

export default HelpDialog;
