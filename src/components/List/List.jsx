import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'row',
  },
  row: {
    minHeight: 16,
  },
};

const List = ({
  rows,
  data,
  spacing,
  grid,
  showEmpty,
  classes,
}) => {
  const keys = Object.keys(rows);
  const values = [];

  const setItems = (label, value) => {
    /* eslint-disable function-paren-newline */
    values.push(
      <Grid key={label} item container direction="row" spacing={24}>
        <Grid item md={grid.label}>
          <Typography color="textSecondary">{label}</Typography>
        </Grid>
        <Grid item md={grid.value}>
          <Typography color="textSecondary">{value}</Typography>
        </Grid>
      </Grid>,
    );
    /* eslint-enable function-paren-newline */
  };

  keys.forEach((key) => {
    const label = rows[key];
    const value = data[key];

    if (value) {
      setItems(label, value);
      return;
    }

    if (showEmpty) {
      setItems(label, value);
    }
  });

  return (
    <div className={classes.container}>
      <Grid container direction="column" spacing={spacing}>
        {values}
      </Grid>
    </div>
  );
};

List.propTypes = {
  rows: PropTypes.shape({
    [PropTypes.string]: PropTypes.string,
  }).isRequired,
  data: PropTypes.shape({
    [PropTypes.string]: PropTypes.any,
  }).isRequired,
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  spacing: PropTypes.number,
  showEmpty: PropTypes.bool,
  grid: PropTypes.shape({
    label: PropTypes.number,
    value: PropTypes.number,
  }),
};

List.defaultProps = {
  spacing: 16,
  showEmpty: false,
  grid: { label: 5, value: 7 },
};

export default withStyles(styles)(List);
