import React from 'react';
import PropTypes from 'prop-types';

import { Modal as ObjectModal } from '../Object';

const Content = ({
  lot,
  objectType,
}) => <ObjectModal.Body objectType={objectType} lot={lot} />;

Content.propTypes = {
  objectType: PropTypes.string.isRequired,
  lot: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default Content;
