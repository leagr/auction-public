import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import Typography from '@material-ui/core/Typography';
import blue from '@material-ui/core/colors/blue';

import Price from '../Price';
import Action from '../Action';
import Timer from '../Timer';
import MoreMenu from '../MoreMenu';
import { Actions as ObjectActions } from '../Object';

const styles = theme => ({
  font: theme.typography.subheading,
  moreButton: {
    marginLeft: 40,
  },
  root: {
    minHeight: 48,
  },
});

const Header = ({
  classes,
  state,
  lot,
  getLot,
  objectType,
  isLoading,
  isConnected,
}) => (
  <Grid
    className={classNames(classes.font, classes.root)}
    container
    direction="row"
    wrap="nowrap"
    alignItems="center"
    justify="space-between"
  >
    {
      state !== 'completed' && (
        <Grid item container direction="row" spacing={40} alignItems="center" wrap="nowrap">
          <Grid item>
            <Timer lot={lot} />
          </Grid>
          {/*<Grid item container direction="row" spacing={8}>*/}
            {/*{*/}
              {/*state === 'active' && Boolean(lot.currentPrice) && (*/}
                {/*<React.Fragment>*/}
                  {/*<Grid item>*/}
                    {/*<Typography>Текущая ставка:</Typography>*/}
                  {/*</Grid>*/}
                  {/*<Grid item>*/}
                    {/*<Typography>*/}
                      {/*<Price price={lot.currentPrice} />*/}
                    {/*</Typography>*/}
                  {/*</Grid>*/}
                {/*</React.Fragment>*/}
              {/*)*/}
            {/*}*/}
            {/*{*/}
              {/*Boolean(lot.userPrice) && (*/}
                {/*<React.Fragment>*/}
                  {/*<Grid item>*/}
                    {/*<Typography>Ваша ставка:</Typography>*/}
                  {/*</Grid>*/}
                  {/*<Grid item>*/}
                    {/*<Typography><Price price={lot.userPrice} color={blue[500]} /></Typography>*/}
                  {/*</Grid>*/}
                {/*</React.Fragment>*/}
              {/*)*/}
            {/*}*/}
          {/*</Grid>*/}
        </Grid>
      )
    }
    {/*<Grid item className={classes.moreButton}>*/}
      {/*{!lot.isWait && (*/}
        {/*<MoreMenu*/}
          {/*lot={lot}*/}
          {/*state={state}*/}
          {/*objectType={objectType}*/}
          {/*isLoading={isLoading}*/}
          {/*isConnected={isConnected}*/}
          {/*getLot={getLot}*/}
          {/*icon={<MenuIcon />}*/}
        {/*>*/}
          {/*<Action*/}
            {/*state={state}*/}
            {/*lot={lot}*/}
            {/*getLot={getLot}*/}
            {/*objectType={objectType}*/}
            {/*isLoading={isLoading}*/}
            {/*isConnected={isConnected}*/}
          {/*/>*/}
          {/*<ObjectActions.FileDownloadAction*/}
            {/*objectType={objectType}*/}
            {/*isBooked={lot.isBooked}*/}
            {/*url={lot.object.docs_pack_url}*/}
          {/*/>*/}
        {/*</MoreMenu>*/}
      {/*)}*/}
    {/*</Grid>*/}
  </Grid>
);

Header.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  state: PropTypes.string.isRequired,
  lot: PropTypes.objectOf(PropTypes.any).isRequired,
  getLot: PropTypes.func.isRequired,
  objectType: PropTypes.string.isRequired,
  isLoading: PropTypes.bool.isRequired,
  isConnected: PropTypes.bool.isRequired,
};

export default withStyles(styles)(Header);
