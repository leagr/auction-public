import React from 'react';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import { observer } from 'mobx-react';
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Button,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import grey from '@material-ui/core/colors/grey';

import Header from './Header';
import Content from './Content';

const styles = theme => ({
  dialogActions: {
    padding: 0,
    margin: 0,
    height: theme.spacing.unit * 2,
  },
  dialogContent: {
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing.unit * 3,
  },
  dialogTitle: {
    borderBottom: `0.5px solid ${grey[300]}`,
    padding: '8px 24px',
  },
  closeButton: {
    margin: 0,
  },
  dialog: {
    width: 768,
    maxWidth: '100vw',
    left: 'calc((100vw - 800px) / 2)',
  },
});

const LotModal = ({
  classes,
  lot,
  getLot,
  onClose,
  state,
  objectType,
  isLoading,
  isConnected,
  open,
}) => (
  <Dialog
    fullWidth
    open={open}
    onClose={onClose}
    maxWidth={false}
    className={classes.dialog}
  >
    <DialogTitle className={classes.dialogTitle}>
      <Header
        lot={lot}
        getLot={getLot}
        onClose={onClose}
        state={state}
        objectType={objectType}
        isLoading={isLoading}
        isConnected={isConnected}
      />
    </DialogTitle>
    <DialogContent className={classes.dialogContent}>
      <Content lot={lot} objectType={objectType} />
    </DialogContent>
    <DialogActions>
      <Button onClick={onClose} color="primary">
        Закрыть
      </Button>
    </DialogActions>
  </Dialog>
);

LotModal.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  lot: PropTypes.objectOf(PropTypes.any),
  getLot: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  state: PropTypes.string.isRequired,
  objectType: PropTypes.string.isRequired,
  isLoading: PropTypes.bool.isRequired,
  isConnected: PropTypes.bool.isRequired,
  open: PropTypes.bool.isRequired,
};

LotModal.defaultProps = {
  lot: null,
};

export default compose(
  withStyles(styles),
  observer,
)(LotModal);
