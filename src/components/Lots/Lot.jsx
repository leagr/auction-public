import React from 'react';
import PropTypes from 'prop-types';
import TableRow from '@material-ui/core/TableRow';
import blue from '@material-ui/core/colors/blue';
import MoreVertIcon from '@material-ui/icons/MoreVert';

import TableCell from '../TableCell';
import Price from '../Price';
import {
  Actions as ObjectActions,
  Table as ObjectTable,
} from '../Object';
import Timer from '../Timer';
import Action from '../Action';
import LotMenuItem from './LotMenuItem';
import MoreMenu from '../MoreMenu';
import * as states from '../../constants/states';

const Lot = ({
  objectType,
  state,
  lot,
  checkUser,
  getLot,
  isLoading,
  isConnected,
  lotIsLoading,
}) => {
  // eslint-disable-next-line react/prop-types
  const renderLotMenuItem = ({ onClose }) => (
    <LotMenuItem
      key="menu_item"
      lot={lot}
      state={state}
      objectType={objectType}
      isLoading={isLoading}
      isConnected={isConnected}
      getLot={getLot}
      onClose={onClose}
    />
  );

  return (
    <TableRow hover>
      <TableCell numeric>{lot.id}</TableCell>
      <TableCell show={state !== states.COMPLETED} >
        <Timer lot={lot} />
      </TableCell>
      <ObjectTable.Body objectType={objectType} lot={lot} state={state} />
      <TableCell numeric show={state === states.ACTIVE}>
        {
          Boolean(lot.basePrice) &&
          <Price price={lot.basePrice} />
        }
      </TableCell>
      <TableCell numeric>
        {
          Boolean(lot.currentPrice) && !lot.isPick &&
          <Price price={lot.currentPrice} />
        }
      </TableCell>
      <TableCell numeric show={state === states.ACTIVE}>
        {
          Boolean(lot.userPrice) &&
          <Price color={checkUser(lot.bet.user_id) ? blue[500] : undefined} price={lot.userPrice} />
        }
      </TableCell>
      <TableCell show={state === states.ACTIVE}>
        {!lot.isWait && (
          <Action
            key="action"
            state={state}
            lot={lot}
            objectType={objectType}
            isLoading={isLoading}
            lotIsLoading={lotIsLoading}
            isConnected={isConnected}
            getLot={getLot}
            type="button"
          />
        )}
      </TableCell>
      <TableCell show={state === states.ACTIVE}>
        <MoreMenu
          lot={lot}
          state={state}
          objectType={objectType}
          isLoading={isLoading}
          isConnected={isConnected}
          getLot={getLot}
          icon={<MoreVertIcon />}
          renderItems={renderLotMenuItem}
        />
      </TableCell>
      <TableCell show={state !== states.ACTIVE} >
        <MoreMenu
          lot={lot}
          state={state}
          objectType={objectType}
          isLoading={isLoading}
          isConnected={isConnected}
          getLot={getLot}
          icon={<MoreVertIcon />}
          renderItems={({ onClose }) => {
            const items = lot.isWait ? [] : [
              <Action
                key="action"
                state={state}
                lot={lot}
                objectType={objectType}
                isLoading={isLoading}
                lotIsLoading={lotIsLoading}
                isConnected={isConnected}
                getLot={getLot}
                onClose={onClose}
              />,
              <ObjectActions.FileDownloadAction
                key="fileDownloadAction"
                objectType={objectType}
                isBooked={lot.isBooked}
                url={lot.object.docs_pack_url}
              />,
            ];

            return [...items, renderLotMenuItem({ onClose })];
          }}
        />
      </TableCell>
    </TableRow>
  );
};

Lot.propTypes = {
  objectType: PropTypes.string.isRequired,
  state: PropTypes.string.isRequired,
  checkUser: PropTypes.func.isRequired,
  getLot: PropTypes.func.isRequired,
  lot: PropTypes.shape({
    id: PropTypes.number.isRequired,
    isWait: PropTypes.bool.isRequired,
  }).isRequired,
  isLoading: PropTypes.bool.isRequired,
  lotIsLoading: PropTypes.bool.isRequired,
  isConnected: PropTypes.bool.isRequired,
};

export default Lot;
