import React from 'react';
import PropTypes from 'prop-types';

import LotModal from '../LotModal';
import MenuItemBase from '../Action/MenuItemBase';

const LotMenuItem = ({
  state,
  objectType,
  lot,
  isConnected,
  isLoading,
  getLot,
  onClose,
}) => (
  <MenuItemBase
    onOpen={() => getLot(lot.id)}
    lot={lot}
    label="Подробнее"
    isLoading={isLoading}
    isConnected={isConnected}
    Dialog={LotModal}
    DialogProps={{
      state,
      objectType,
      isConnected,
      getLot,
      onClose,
    }}
  />
);

LotMenuItem.propTypes = {
  lot: PropTypes.objectOf(PropTypes.any).isRequired,
  getLot: PropTypes.func.isRequired,
  objectType: PropTypes.string.isRequired,
  state: PropTypes.string.isRequired,
  isLoading: PropTypes.bool.isRequired,
  isConnected: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default LotMenuItem;
