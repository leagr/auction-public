import React from 'react';
import PropTypes from 'prop-types';
import {
  Table,
  TableHead,
  TableRow,
  TableBody,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import TableCell from '../TableCell';
import * as states from '../../constants/states';
import Preloader from '../Preloader';
import ConnectionLost from '../ConnectionLost';
import { Table as ObjectTable } from '../Object';
import Lot from './Lot';

const styles = {
  tablePreloader: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  tableRoot: {
    width: '100%',
    overflowY: 'auto',
    overflowX: 'auto',
  },
};

const Lots = ({
  objectType,
  state,
  lots,
  checkUser,
  getLot,
  classes,
  isLoading,
  lotIsLoading,
  isConnected,
}) => {
  const renderTable = () => (
    <div className={classes.tableRoot}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell numeric>Номер</TableCell>
            <TableCell show={state !== states.COMPLETED}>
              Статус
            </TableCell>
            <ObjectTable.Head objectType={objectType} state={state} />
            <TableCell numeric show={state === states.ACTIVE}>
              Базовая стоимость
            </TableCell>
            <TableCell numeric show={state === states.ACTIVE}>
              Текущая стоимость
            </TableCell>
            <TableCell numeric>
              {state === states.ACTIVE ? 'Ваша ставка' : 'Стоимость'}
            </TableCell>
            <TableCell show={state === states.ACTIVE} />
            <TableCell />
          </TableRow>
        </TableHead>
        <TableBody>
          {lots.map(lot => (
            <Lot
              key={lot.id}
              objectType={objectType}
              state={state}
              lot={lot}
              checkUser={checkUser}
              getLot={getLot}
              isLoading={isLoading}
              isConnected={isConnected}
              lotIsLoading={lotIsLoading}
            />
          ))}
        </TableBody>
      </Table>
    </div>
  );

  const preloader = (
    <div className={classes.tablePreloader}>
      <Preloader size={25} width="100%" />
    </div>
  );

  return (
    <React.Fragment>
      <ConnectionLost
        isConnected={isConnected}
        text="Соединение потеряно. Ожидайте повторного подключения."
      />
      {
        !isLoading && isConnected ?
          renderTable() :
          preloader
      }
    </React.Fragment>
  );
};

Lots.propTypes = {
  objectType: PropTypes.string.isRequired,
  state: PropTypes.string.isRequired,
  lots: PropTypes.arrayOf(PropTypes.object).isRequired,
  checkUser: PropTypes.func.isRequired,
  getLot: PropTypes.func.isRequired,
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  isLoading: PropTypes.bool.isRequired,
  lotIsLoading: PropTypes.bool.isRequired,
  isConnected: PropTypes.bool.isRequired,
};

export default withStyles(styles)(Lots);
