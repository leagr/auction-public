/* eslint-disable react/no-typos */
import React from 'react';
import PropTypes from 'prop-types';
import { PropTypes as MobxPropTypes } from 'mobx-react';
import { compose, withStateHandlers } from 'recompose';
import {
  Popover,
  MenuItem,
  Typography,
  withStyles,
} from '@material-ui/core';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

const styles = theme => ({
  container: {
    alignItems: 'center',
    cursor: 'pointer',
    display: 'flex',
    marginLeft: theme.spacing.unit * 3,
    outline: 'none',
  },
  icon: {
    marginLeft: theme.spacing.unit,
  },
  groupName: {
    cursor: 'pointer',
  },
});

const GroupsMenu = ({
  anchorEl,
  classes,
  groups,
  handleClose,
  handleOpen,
  onSelect,
  value,
}) => (
  <React.Fragment>
    <div
      className={classes.container}
      onClick={event => handleOpen(event.currentTarget)}
      onKeyPress={event => handleOpen(event.currentTarget)}
      role="button"
      tabIndex={0}
    >
      <Typography variant="subheading" className={classes.groupName}>
        {groups.filter(group => group.key === value)[0].name}
      </Typography>
      {anchorEl ?
        <KeyboardArrowUpIcon className={classes.icon} /> :
        <KeyboardArrowDownIcon className={classes.icon} />
      }
    </div>
    <Popover
      anchorEl={anchorEl}
      open={Boolean(anchorEl)}
      onClick={handleClose}
      onClose={handleClose}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
    >
      {
        groups.map(group => (
          <MenuItem
            key={group.key}
            onClick={() => onSelect(group.key)}
          >
            {group.name}
          </MenuItem>
        ))
      }
    </Popover>
  </React.Fragment>
);

GroupsMenu.propTypes = {
  anchorEl: PropTypes.objectOf(PropTypes.any),
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  groups: MobxPropTypes.arrayOrObservableArrayOf(PropTypes.shape({
    key: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  })).isRequired,
  handleClose: PropTypes.func.isRequired,
  handleOpen: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
};

GroupsMenu.defaultProps = {
  anchorEl: null,
};

const enhance = withStateHandlers(
  () => {},
  {
    handleClose: () => () => ({ anchorEl: null }),
    handleOpen: () => anchorEl => ({ anchorEl }),
  },
);

export default compose(
  withStyles(styles),
  enhance,
)(GroupsMenu);
