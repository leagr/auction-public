/* eslint-disable react/no-typos */
import React from 'react';
import PropTypes from 'prop-types';
import { observer, PropTypes as MobxPropTypes } from 'mobx-react';
import { Paper } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import GroupMenu from './GroupsMenu';
import StateMenu from './StateMenu';

const styles = {
  container: {
    alignItems: 'center',
    display: 'flex',
    flexGrow: 1,
    height: 48,
    padding: '0 28px',
    position: 'relative',
    maxHeight: 48,
  },
};

const LotsHeader = ({
  classes,
  filters,
  groups,
  setFilter,
}) => (
  <Paper className={classes.container}>
    <GroupMenu
      groups={groups}
      onSelect={value => setFilter('group_key', value)}
      value={filters.group_key}
    />
    <StateMenu
      onSelect={value => setFilter('state', value)}
      value={filters.state}
    />
  </Paper>
);

LotsHeader.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  filters: PropTypes.shape({
    group_key: PropTypes.string,
    state: PropTypes.string,
  }).isRequired,
  groups: MobxPropTypes.arrayOrObservableArrayOf(PropTypes.object).isRequired,
  setFilter: PropTypes.func.isRequired,
};

export default observer(withStyles(styles)(LotsHeader));
