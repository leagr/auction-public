import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { Tab, Tabs } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const STATE_LABELS = {
  active: 'Активные',
  booked: 'Забронированные',
  completed: 'Завершенные',
};

const styles = theme => ({
  tabs: {
    flexGrow: 1,
    marginRight: 100 + (theme.spacing.unit * 3),
  },
});

const StateMenu = ({
  classes,
  onSelect,
  value,
}) => (
  <Tabs
    centered
    className={classes.tabs}
    value={value}
    onChange={(event, v) => onSelect(v)}
  >
    {Object.keys(STATE_LABELS).map(key => (
      <Tab key={key} label={STATE_LABELS[key]} value={key} />
    ))}
  </Tabs>
);

StateMenu.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  onSelect: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
};

export default observer(withStyles(styles)(StateMenu));
