import React from 'react';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';
import Menu from '@material-ui/core/Menu';
import IconButton from '@material-ui/core/IconButton';

const enhance = compose(
  withState('anchorEl', 'setAnchorEl', null),
  withHandlers({
    handleOpen: ({ setAnchorEl }) => e => setAnchorEl(e.currentTarget),
    handleClose: ({ setAnchorEl }) => () => setAnchorEl(null),
  }),
);

const MoreMenu = enhance(({
  anchorEl,
  handleOpen,
  handleClose,
  icon,
  renderItems,
}) => (
  <React.Fragment>
    <IconButton onClick={handleOpen}>
      {icon}
    </IconButton>
    <Menu
      disableAutoFocusItem
      anchorEl={anchorEl}
      open={Boolean(anchorEl)}
      onClose={handleClose}
    >
      {renderItems({ onClose: handleClose })}
    </Menu>
  </React.Fragment>
));

MoreMenu.propTypes = {
  lot: PropTypes.objectOf(PropTypes.any).isRequired,
  getLot: PropTypes.func.isRequired,
  objectType: PropTypes.string.isRequired,
  state: PropTypes.string.isRequired,
  isLoading: PropTypes.bool.isRequired,
  isConnected: PropTypes.bool.isRequired,
  icon: PropTypes.node.isRequired,
};

export default MoreMenu;
