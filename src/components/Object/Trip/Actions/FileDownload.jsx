import React from 'react';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import branch from 'recompose/branch';
import renderNothing from 'recompose/renderNothing';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  a: {
    textDecoration: 'none',
  },
};

const FileDownload = ({ url, classes }) => (
  <MenuItem>
    <a download href={url} className={classes.a}>
      <Typography variant="button">
        Пакет документов
      </Typography>
    </a>
  </MenuItem>
);

FileDownload.propTypes = {
  url: PropTypes.string.isRequired,
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
};

export default compose(
  withStyles(styles),
  branch(
    ({ isBooked }) => !isBooked,
    renderNothing,
  ),
)(FileDownload);
