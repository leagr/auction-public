import FileDownload from './FileDownload';

const Actions = { FileDownloadAction: FileDownload };

export default Actions;
