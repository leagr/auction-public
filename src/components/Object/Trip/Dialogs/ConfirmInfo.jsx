import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import branch from 'recompose/branch';
import renderNothing from 'recompose/renderNothing';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';

import { SERVER_DATE_TIME_FORMAT } from '../../../../constants/date';

const styles = theme => ({
  dialog: {
    width: 600,
    left: 'calc((100vw - 400px) / 2)',
  },
  item: {
    minHeight: 20,
  },
  secondaryLabel: {
    marginLeft: theme.spacing.unit,
  },
});

const ConfirmInfo = ({
  confirm,
  title,
  onClose,
  classes,
  open,
}) => (
  <Dialog
    open={open}
    onClose={onClose}
    fullWidth
    maxWidth={false}
    className={classes.dialog}
  >
    <DialogTitle>{title}</DialogTitle>
    <DialogContent>
      <Grid container direction="row" justify="space-between">
        <Grid container item direction="column" spacing={16} xs={6}>
          <Grid item>
            <Typography color="textSecondary">Фамилия</Typography>
          </Grid>
          <Grid item>
            <Typography color="textSecondary">Имя</Typography>
          </Grid>
          <Grid item>
            <Typography color="textSecondary">Отчество</Typography>
          </Grid>
          <Grid item>
            <Typography color="textSecondary">Номер телефона</Typography>
          </Grid>
          <Grid item>
            <Typography color="textSecondary">Паспорт:</Typography>
          </Grid>
          <Grid item>
            <Typography className={classes.secondaryLabel} color="textSecondary">серия</Typography>
          </Grid>
          <Grid item>
            <Typography className={classes.secondaryLabel} color="textSecondary">номер</Typography>
          </Grid>
          <Grid item>
            <Typography className={classes.secondaryLabel} color="textSecondary">дата выдачи</Typography>
          </Grid>
          <Grid item>
            <Typography className={classes.secondaryLabel} color="textSecondary">кем выдан</Typography>
          </Grid>
          <Grid item>
            <Typography color="textSecondary">Водительское удостоверение:</Typography>
          </Grid>
          <Grid item>
            <Typography className={classes.secondaryLabel} color="textSecondary">серия</Typography>
          </Grid>
          <Grid item>
            <Typography className={classes.secondaryLabel} color="textSecondary">номер</Typography>
          </Grid>
          <Grid item>
            <Typography color="textSecondary">Номер машины</Typography>
          </Grid>
          <Grid item>
            <Typography color="textSecondary">Номер прицепа</Typography>
          </Grid>
          <Grid item>
            <Typography color="textSecondary">Дата прибытия</Typography>
          </Grid>
        </Grid>
        <Grid container item direction="column" spacing={16} xs={6}>
          <Grid item>
            <Typography className={classes.item} color="textSecondary">{confirm.surname}</Typography>
          </Grid>
          <Grid item>
            <Typography className={classes.item} color="textSecondary">{confirm.name}</Typography>
          </Grid>
          <Grid item>
            <Typography className={classes.item} color="textSecondary">{confirm.patronymic}</Typography>
          </Grid>
          <Grid item>
            <Typography className={classes.item} color="textSecondary">{confirm.phone_number}</Typography>
          </Grid>
          <Grid item>
            <Typography className={classes.item} />
          </Grid>
          <Grid item>
            <Typography className={classes.item} color="textSecondary">{confirm.passport_serie}</Typography>
          </Grid>
          <Grid item>
            <Typography className={classes.item} color="textSecondary">{confirm.passport_number}</Typography>
          </Grid>
          <Grid item>
            <Typography className={classes.item} color="textSecondary">
              {
                confirm.passport_date && moment.utc(confirm.passport_date, SERVER_DATE_TIME_FORMAT)
                  .format('LLL')
              }
            </Typography>
          </Grid>
          <Grid item>
            <Typography className={classes.item} color="textSecondary">{confirm.passport_issued}</Typography>
          </Grid>
          <Grid item>
            <Typography className={classes.item} />
          </Grid>
          <Grid item>
            <Typography className={classes.item} color="textSecondary">{confirm.dr_lic_serie}</Typography>
          </Grid>
          <Grid item>
            <Typography className={classes.item} color="textSecondary">{confirm.dr_lic_number}</Typography>
          </Grid>
          <Grid item>
            <Typography className={classes.item} color="textSecondary">{confirm.truck_number }</Typography>
          </Grid>
          <Grid item>
            <Typography className={classes.item} color="textSecondary">{confirm.trailer_number}</Typography>
          </Grid>
          <Grid item>
            <Typography className={classes.item} color="textSecondary">
              {
                moment.utc(confirm.date_arrival, SERVER_DATE_TIME_FORMAT).local()
                  .format('LLL')
              }
            </Typography>
          </Grid>
        </Grid>
      </Grid>
    </DialogContent>
    <DialogActions>
      <Button onClick={onClose} color="primary">Закрыть</Button>
    </DialogActions>
  </Dialog>
);

ConfirmInfo.propTypes = {
  confirm: PropTypes.shape({
    surname: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    patronymic: PropTypes.string.isRequired,
    phone_number: PropTypes.string.isRequired,
    passport_serie: PropTypes.string,
    passport_number: PropTypes.string,
    passport_issued: PropTypes.string,
    passport_date: PropTypes.string,
    dr_lic_number: PropTypes.string,
    dr_lic_serie: PropTypes.string,
    truck_number: PropTypes.string,
    trailer_number: PropTypes.string,
    date_arrival: PropTypes.string,
  }).isRequired,
  title: PropTypes.string.isRequired,
  onClose: PropTypes.func.isRequired,
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  open: PropTypes.bool.isRequired,
};

export default branch(
  ({ confirm }) => !confirm,
  renderNothing,
)(withStyles(styles)(ConfirmInfo));
