import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import compose from 'recompose/compose';
import branch from 'recompose/branch';
import renderNothing from 'recompose/renderNothing';
import Yup from 'yup';
import { withFormik } from 'formik';
import { withStyles } from '@material-ui/core/styles';
import {
  Dialog,
  DialogContent,
  DialogActions,
  DialogTitle,
  TextField,
  Button,
  Typography,
  Grid,
} from '@material-ui/core';

import { checkPhoneNumber } from '../../../../core/utils';
import PhoneNumberInput from '../../../PhoneNumberInput';
import DateTimeInput from '../../../DateTimeInput';
import { CLIENT_DATE_TIME_FORMAT, SERVER_DATE_TIME_FORMAT } from '../../../../constants/date';

const styles = {
  form: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  textField: {
    flex: 1,
    padding: '8px 0',
  },
};

const enhance = branch(
  ({ lotIsLoading }) => !lotIsLoading,
  compose(
    withFormik({
      displayName: 'ConfirmLot',
      // eslint-disable-next-line no-confusing-arrow
      mapPropsToValues: ({ confirm }) => confirm ? ({
        ...confirm,
        phone_number: checkPhoneNumber(confirm.phone_number)
          ? confirm.phone_number.match(/\d/g).join('') : '',
        date_arrival: moment.utc(confirm.date_arrival, SERVER_DATE_TIME_FORMAT)
          .local().format(CLIENT_DATE_TIME_FORMAT),
        passport_date: confirm.passport_date ?
          moment(confirm.passport_date, SERVER_DATE_TIME_FORMAT).format(CLIENT_DATE_TIME_FORMAT) : '',
      }) : ({
        surname: '',
        name: '',
        patronymic: '',
        truck_number: '',
        trailer_number: '',
        date_arrival: '',
        phone_number: '',
        passport_serie: '',
        passport_number: '',
        passport_issued: '',
        passport_date: '',
        dr_lic_serie: '',
        dr_lic_number: '',
      }),
      validationSchema: Yup.object().shape({
        surname: Yup.string()
          .required('Введите фамилию'),
        name: Yup.string()
          .required('Введите имя'),
        patronymic: Yup.string()
          .required('Введите отчество'),
        phone_number: Yup.string()
          .required('Введите номер телефона'),
        truck_number: Yup.string()
          .required('Введите номер машины'),
        trailer_number: Yup.string()
          .required('Введите номер прицепа'),
        date_arrival: Yup.string()
          .test(
            'date_arrival',
            'Некорректное значение',
            // eslint-disable-next-line no-confusing-arrow
            value => value && value !== '' ? moment(value, CLIENT_DATE_TIME_FORMAT).isValid() : true,
          )
          .required('Введите дату прибытия'),
        passport_date: Yup.string()
          .test(
            'passport_date',
            'Некорректное значение',
            // eslint-disable-next-line no-confusing-arrow
            value => value && value !== '' ? moment(value, CLIENT_DATE_TIME_FORMAT).isValid() : true,
          ),
      }),
      handleSubmit: async (values, { props: { onSubmit, lot } }) => {
        const { date_arrival: dateArrival, passport_date: passportDate } = values;

        const info = {
          ...values,
          date_arrival: dateArrival ?
            moment(dateArrival, CLIENT_DATE_TIME_FORMAT).utc().format() :
            undefined,
          passport_date: passportDate ?
            moment.utc(passportDate, CLIENT_DATE_TIME_FORMAT).format() :
            undefined,
        };

        await onSubmit({
          lot_id: lot.id,
          info,
        });

        await lot.setConfirm(info);
      },
    }),
    withStyles(styles),
  ),
  renderNothing,
);

const ConfirmLot = enhance(({
  onClose,
  handleChange,
  values,
  errors,
  touched,
  handleSubmit,
  title,
  classes,
  open,
}) => {
  // console.log(values.phone_number.match(/[0-9]/g))
  // eslint-disable-next-line react/prop-types
  const renderField = ({ value, ...rest }) => (
    <TextField
      id={value}
      error={Boolean(touched[value] && errors[value])}
      helperText={touched[value] && errors[value]}
      value={values[value] || ''}
      {...rest}
    />
  );

  return (
    <Dialog onKeyDown={e => e.stopPropagation()} open={open} onClose={onClose} fullWidth maxWidth="sm">
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>
        <form
          className={classes.form}
          id="place_bet_dialog_form"
          onSubmit={handleSubmit}
        >
          <Grid container direction="column" spacing={40} justify="center">
            <Grid item container direction="row" spacing={8}>
              <Grid item md={4} sm={4}>
                {renderField({
                  className: classes.textField,
                  value: 'surname',
                  label: 'Фамилия',
                  fullWidth: true,
                  onChange: handleChange,
                })}
              </Grid>
              <Grid item md={4} sm={4}>
                {renderField({
                  className: classes.textField,
                  value: 'name',
                  label: 'Имя',
                  fullWidth: true,
                  onChange: handleChange,
                })}
              </Grid>
              <Grid item md={4} sm={4}>
                {renderField({
                  className: classes.textField,
                  value: 'patronymic',
                  label: 'Отчество',
                  fullWidth: true,
                  onChange: handleChange,
                })}
              </Grid>
              <Grid item md={4} sm={4}>
                {renderField({
                  className: classes.textField,
                  value: 'phone_number',
                  label: 'Номер телефона',
                  fullWidth: true,
                  onChange: handleChange,
                  InputProps: {
                    inputComponent: PhoneNumberInput,
                  },
                  InputLabelProps: {
                    shrink: Boolean(values.phone_number),
                  },
                })}
              </Grid>
            </Grid>
            <Grid container item spacing={8} direction="row">
              <Grid item span={2}>
                <Typography variant="subheading" color="textSecondary">
                  Паспорт:
                </Typography>
              </Grid>
              <Grid item container spacing={8} direction="row">
                <Grid item md={4} sm={4}>
                  {renderField({
                    className: classes.textField,
                    value: 'passport_serie',
                    label: 'Серия',
                    fullWidth: true,
                    onChange: handleChange,
                  })}
                </Grid>
                <Grid item md={4} sm={4}>
                  {renderField({
                    className: classes.textField,
                    value: 'passport_number',
                    label: 'Номер',
                    fullWidth: true,
                    onChange: handleChange,
                  })}
                </Grid>
                <Grid item md={4} sm={4}>
                  {renderField({
                    className: classes.textField,
                    value: 'passport_date',
                    label: 'Дата выдачи',
                    fullWidth: true,
                    onChange: handleChange,
                    InputProps: {
                      inputComponent: DateTimeInput,
                    },
                    InputLabelProps: {
                      shrink: Boolean(values.passport_date),
                    },
                  })}
                </Grid>
                <Grid item md={8} sm={8}>
                  {renderField({
                    className: classes.textField,
                    value: 'passport_issued',
                    label: 'Кем выдан',
                    fullWidth: true,
                    onChange: handleChange,
                  })}
                </Grid>
              </Grid>
            </Grid>
            <Grid container item spacing={8} direction="column">
              <Grid item>
                <Typography variant="subheading" color="textSecondary">
                  Водительское удостоверение:
                </Typography>
              </Grid>
              <Grid item container spacing={8} direction="row">
                <Grid item md={4} sm={4}>
                  {renderField({
                    className: classes.textField,
                    value: 'dr_lic_serie',
                    label: 'Серия',
                    fullWidth: true,
                    onChange: handleChange,
                  })}
                </Grid>
                <Grid item md={4} sm={4}>
                  {renderField({
                    className: classes.textField,
                    value: 'dr_lic_number',
                    label: 'Номер',
                    fullWidth: true,
                    onChange: handleChange,
                  })}
                </Grid>
              </Grid>
            </Grid>
            <Grid container item spacing={8} direction="row">
              <Grid item md={4} sm={4}>
                {renderField({
                  className: classes.textField,
                  value: 'truck_number',
                  label: 'Номер машины',
                  fullWidth: true,
                  onChange: handleChange,
                })}
              </Grid>
              <Grid item md={4} sm={4}>
                {renderField({
                  className: classes.textField,
                  value: 'trailer_number',
                  label: 'Номер прицепа',
                  fullWidth: true,
                  onChange: handleChange,
                })}
              </Grid>
              <Grid item md={4} sm={4}>
                {renderField({
                  className: classes.textField,
                  value: 'date_arrival',
                  label: 'Дата прибытия',
                  fullWidth: true,
                  onChange: handleChange,
                  inputProps: {
                    time: true,
                  },
                  InputProps: {
                    inputComponent: DateTimeInput,
                  },
                  InputLabelProps: {
                    shrink: Boolean(values.date_arrival),
                  },
                })}
              </Grid>
            </Grid>
          </Grid>
        </form>
      </DialogContent>
      <DialogActions>
        <Button color="primary" onClick={onClose}>Отменить</Button>
        <Button
          type="submit"
          form="place_bet_dialog_form"
          color="primary"
        >
          Отправить
        </Button>
      </DialogActions>
    </Dialog>
  );
});

ConfirmLot.propTypes = {
  onClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
};

export default ConfirmLot;
