import ConfirmLot from './ConfirmLot';
import ConfirmInfo from './ConfirmInfo';

const Dialogs = {
  ConfirmLotDialog: ConfirmLot,
  ConfirmInfoDialog: ConfirmInfo,
};

export default Dialogs;
