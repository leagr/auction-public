import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';

import * as states from '../../../../constants/states';
import DateTimeInput from '../../../DateTimeInput';

const Fields = ({
  values,
  errors,
  handleChange,
  state,
  classes,
}) => (
  <React.Fragment>
    <Grid item xs={4} className={classes.filterItem}>
      <TextField
        id="city_from"
        fullWidth
        placeholder="Откуда"
        margin="none"
        onChange={handleChange}
        value={values.city_from || ''}
      />
    </Grid>
    <Grid item xs={4} className={classes.filterItem}>
      <TextField
        id="city_to"
        fullWidth
        placeholder="Куда"
        margin="none"
        onChange={handleChange}
        value={values.city_to || ''}
      />
    </Grid>
    {
      (state === states.COMPLETED || state === states.BOOKED) && (
        <Grid item xs={4} className={classes.filterItem}>
          <TextField
            id="driver"
            fullWidth
            placeholder="Водитель"
            margin="none"
            onChange={handleChange}
            value={values.driver || ''}
          />
        </Grid>
      )
    }
    {
      state === states.COMPLETED && (
        <Grid item xs={4} className={classes.filterItem}>
          <TextField
            id="invoice"
            fullWidth
            placeholder="Номер накладной"
            margin="none"
            onChange={handleChange}
            value={values.invoice || ''}
          />
        </Grid>
      )
    }
    <Grid item xs={2} className={classes.filterItem}>
      <TextField
        id="from_start_date"
        helperText="Дата погрузки от"
        fullWidth
        value={values.from_start_date || ''}
        error={Boolean(errors.from_start_date)}
        onChange={handleChange}
        InputProps={{
          inputComponent: DateTimeInput,
        }}
      />
    </Grid>
    <Grid item xs={2} className={classes.filterItem}>
      <TextField
        id="from_end_date"
        helperText="до"
        fullWidth
        value={values.from_end_date || ''}
        error={Boolean(errors.from_end_date)}
        onChange={handleChange}
        InputProps={{
          inputComponent: DateTimeInput,
        }}
      />
    </Grid>
    <Grid item xs={2} className={classes.filterItem}>
      <TextField
        id="to_start_date"
        helperText="Дата разгрузки от"
        fullWidth
        value={values.to_start_date || ''}
        error={Boolean(errors.to_start_date)}
        onChange={handleChange}
        InputProps={{
          inputComponent: DateTimeInput,
        }}
      />
    </Grid>
    <Grid item xs={2} className={classes.filterItem}>
      <TextField
        id="to_end_date"
        helperText="до"
        fullWidth
        value={values.to_end_date || ''}
        error={Boolean(errors.to_end_date)}
        onChange={handleChange}
        InputProps={{
          inputComponent: DateTimeInput,
        }}
      />
    </Grid>
  </React.Fragment>
);

Fields.propTypes = {
  handleChange: PropTypes.func.isRequired,
  values: PropTypes.shape({
    city_from: PropTypes.string,
    city_to: PropTypes.string,
    from_start_date: PropTypes.string,
    from_end_date: PropTypes.string,
  }).isRequired,
  errors: PropTypes.shape({
    from_start_date: PropTypes.string,
    from_end_date: PropTypes.string,
  }).isRequired,
  state: PropTypes.string.isRequired,
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
};

export default Fields;
