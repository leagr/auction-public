import Fields from './Fields';
import normalizer from './normalizer';
import validationSchema from './validationSchema';
import mapPropsToValues from './mapPropsToValues';

const Filter = {
  Fields,
  normalizer,
  validationSchema,
  mapPropsToValues,
};

export default Filter;
