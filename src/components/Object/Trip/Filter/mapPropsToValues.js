import moment from 'moment';

import { SERVER_DATE_TIME_FORMAT, CLIENT_DATE_TIME_FORMAT } from '../../../../constants/date';

export default ({
  from_start_date: fromStartDate,
  from_date_end: fromEndDate,
  to_date_start: toStartDate,
  to_date_end: toEndDate,
  ...values
}) => {
  const dates = {};

  if (fromStartDate) {
    dates.from_start_date = moment.utc(fromStartDate, SERVER_DATE_TIME_FORMAT)
      .local().format(CLIENT_DATE_TIME_FORMAT);
  }

  if (fromEndDate) {
    dates.from_date_end = moment.utc(fromEndDate, SERVER_DATE_TIME_FORMAT)
      .local().format(CLIENT_DATE_TIME_FORMAT);
  }

  if (toStartDate) {
    dates.to_start_date = moment.utc(toStartDate, SERVER_DATE_TIME_FORMAT)
      .local().format(CLIENT_DATE_TIME_FORMAT);
  }

  if (toEndDate) {
    dates.to_end_date = moment.utc(toEndDate, SERVER_DATE_TIME_FORMAT)
      .local().format(CLIENT_DATE_TIME_FORMAT);
  }

  return ({
    ...values,
    ...dates,
  });
};
