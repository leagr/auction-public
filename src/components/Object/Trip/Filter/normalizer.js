import moment from 'moment';

import { CLIENT_DATE_TIME_FORMAT } from '../../../../constants/date';

export default values => ({
  ...values,
  from_start_date: values.from_start_date ?
    moment(values.from_start_date, CLIENT_DATE_TIME_FORMAT).local().format() : undefined,
  from_end_date: values.from_end_date ?
    moment(values.from_end_date, CLIENT_DATE_TIME_FORMAT).local().format() : undefined,
  to_start_date: values.to_start_date ?
    moment(values.to_start_date, CLIENT_DATE_TIME_FORMAT).local().format() : undefined,
  to_end_date: values.to_end_date ?
    moment(values.to_end_date, CLIENT_DATE_TIME_FORMAT).local().format() : undefined,
});
