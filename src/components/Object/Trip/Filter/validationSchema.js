import Yup from 'yup';
import moment from 'moment';

import { CLIENT_DATE_TIME_FORMAT } from '../../../../constants/date';

const testFunction = value => !value ||
  moment(value, CLIENT_DATE_TIME_FORMAT).isValid();

export default {
  from_start_date: Yup.string()
    .test(
      'from_start_date',
      'Некорректное значение',
      testFunction,
    ),
  from_end_date: Yup.string()
    .test(
      'from_end_date',
      'Некорректное значение',
      testFunction,
    ),
  to_start_date: Yup.string()
    .test(
      'to_start_date',
      'Некорректное значение',
      testFunction,
    ),
  to_end_date: Yup.string()
    .test(
      'to_end_date',
      'Некорректное значение',
      testFunction,
    ),
};
