import React from 'react';
import PropTypes from 'prop-types';
import {
  Card,
  CardContent,
  CardHeader,
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  Grid,
  Divider,
  Typography,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import List from '../../../../List';
import Route from './Route';
import YandexMap from '../../../../YandexMap/index';

const ORDER_LABELS = {
  consignee_name: 'Наименование грузополучателя',
  consignee_address: 'Адрес грузополучателя',
  invoices: 'Накладные',
};

const styles = theme => ({
  expansionPanelDetails: {
    padding: 0,
  },
  tonnage: {
    marginBottom: theme.spacing.unit * 2,
  },
  item: {
    marginBottom: theme.spacing.unit * 3,
  },
  divider: {
    marginTop: theme.spacing.unit * 3,
  },
});

const Body = ({ classes, lot }) => {
  const renderRouteMap = () => (
    <ExpansionPanel>
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <Typography variant="subheading">
          На карте
        </Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails className={classes.expansionPanelDetails}>
        <YandexMap points={lot.object.points} />
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );

  const renderDescription = () => (
    <Card>
      <CardHeader
        title={<Typography variant="subheading">Дополнительная информация</Typography>}
      />
      <CardContent>
        <Typography color="textSecondary">
          {lot.object.description}
        </Typography>
      </CardContent>
    </Card>
  );

  const renderOrders = () => {
    const { object: { orders } } = lot;

    return (
      <Card>
        <CardHeader
          title={<Typography variant="subheading">Заявки</Typography>}
        />
        <CardContent>
          <Grid container direction="column" spacing={24}>
            {
              orders
                .map((order, i) => {
                  const data = {
                    ...order,
                    invoices: Object.prototype.hasOwnProperty.call(order, 'invoices')
                      ? order.invoices.join(', ') : undefined,
                  };

                  return (
                    // eslint-disable-next-line react/no-array-index-key
                    <Grid key={i} item>
                      <List rows={ORDER_LABELS} data={data} />
                      {i < orders.length - 1 && <Divider className={classes.divider} />}
                    </Grid>
                  );
                })
            }
          </Grid>
        </CardContent>
      </Card>
    );
  };

  const renderRouteProps = () => (
    <Card>
      <CardHeader
        title={<Typography variant="subheading">Детали маршрута</Typography>}
      />
      <CardContent>
        <Grid container direction="row" spacing={24} className={classes.tonnage}>
          <Grid item>
            <Typography color="textSecondary">Тоннаж:</Typography>
          </Grid>
          <Grid item>
            <Typography color="textSecondary">{lot.object.tonnage}</Typography>
          </Grid>
        </Grid>
        {
          lot.object.type && (
            <Grid container direction="row" spacing={24} className={classes.tonnage}>
              <Grid item>
                <Typography color="textSecondary">Тип транспорта:</Typography>
              </Grid>
              <Grid item>
                <Typography color="textSecondary">{lot.object.type}</Typography>
              </Grid>
            </Grid>
          )
        }
        <Route points={lot.object.points} />
      </CardContent>
    </Card>
  );

  return (
    <React.Fragment>
      <div className={classes.item}>{renderDescription()}</div>
      <div className={classes.item}>{renderOrders()}</div>
      <div className={classes.item}>{renderRouteProps()}</div>
      <div>{renderRouteMap()}</div>
    </React.Fragment>
  );
};

Body.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  lot: PropTypes.shape({
    object: PropTypes.shape({
      points: PropTypes.arrayOf(PropTypes.object).isRequired,
    }).isRequired,
  }).isRequired,
};

export default withStyles(styles)(Body);
