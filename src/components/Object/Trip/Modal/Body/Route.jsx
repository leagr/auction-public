import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import moment from 'moment';
import Avatar from '@material-ui/core/Avatar';
import { withStyles } from '@material-ui/core/styles';
import grey from '@material-ui/core/colors/grey';
import red from '@material-ui/core/colors/red';
import blue from '@material-ui/core/colors/blue';
import green from '@material-ui/core/colors/green';

import List from '../../../../../components/List';

const styles = theme => ({
  container: {
    display: 'flex',
  },
  pointsColumn: {
    display: 'flex',
    flexDirection: 'column',
    width: 64,
    maxWidth: 64,
    flex: 1,
  },
  dataColumn: {
    flex: 1,
  },
  data: {
    margin: theme.spacing.unit * 2,
  },
  verticalLine: {
    border: `0.5px solid ${grey[500]}`,
    flex: 1,
    height: '100%',
    margin: '0 20px',
    width: 0,
  },
  whiteSpace: {
    flex: 1,
    height: '100%',
    width: 0,
  },
  startPoint: {
    background: red[500],
  },
  endPoint: {
    background: blue[500],
  },
  viaPoint: {
    background: green[500],
  },
  point: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
  },
});

const ROUTE_LABELS = {
  address: 'Адрес',
  date: 'Дата',
  company: 'Компания',
  note: 'Примечание',
};

const Route = ({
  classes,
  points,
}) => {
  const renderPoint = (point, i) => (
    <div key={i} className={classes.point}>
      {
        i !== 0 ?
          <div className={classes.verticalLine} /> :
          <div className={classes.whiteSpace} />
      }
      <Avatar
        className={classNames({
          [classes.startPoint]: i === 0,
          [classes.endPoint]: i === points.length - 1,
          [classes.viaPoint]: i !== 0 && i !== points.length - 1,
        })}
      >
        {String.fromCharCode(65 + i)}
      </Avatar>
      {
        i !== points.length - 1 ?
          <div className={classes.verticalLine} /> :
          <div className={classes.whiteSpace} />
      }
    </div>
  );

  const renderData = (point, i) => (
    <div key={i} className={classes.data}>
      <List
        spacing={8}
        grid={{ label: 3, value: 9 }}
        rows={ROUTE_LABELS}
        data={{ ...point, date: moment(point.date).format('LLL') }}
      />
    </div>
  );

  return (
    <div className={classes.container}>
      <div className={classes.pointsColumn}>
        { points.map((point, i) => renderPoint(point, i)) }
      </div>
      <div className={classes.dataColumn}>
        { points.map((point, i) => renderData(point, i)) }
      </div>
    </div>
  );
};

Route.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  points: PropTypes.arrayOf(PropTypes.shape({
    address: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    unloading: PropTypes.bool.isRequired,
    note: PropTypes.string,
    company: PropTypes.string,
  })).isRequired,
};

export default withStyles(styles)(Route);
