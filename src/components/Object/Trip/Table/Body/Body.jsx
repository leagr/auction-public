import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import moment from 'moment';

import TableCell from '../../../../TableCell';
import Route from './Route';
import * as states from '../../../../../constants/states';

const styles = {
  dateTableCell: {
    whiteSpace: 'nowrap',
  },
  routeTableCell: {
    maxWidth: 360,
  },
};

const Body = ({
  classes,
  lot: {
    object: { points, description, tonnage },
    getOrders,
    confirmedAt,
    completedAt,
    driver,
  },
  state,
}) => (
  <Fragment>
    <TableCell className={classes.routeTableCell}>
      <Route points={points} description={description} orders={getOrders} />
    </TableCell>
    <TableCell numeric>
      {tonnage}
    </TableCell>
    <TableCell
      show={state === states.BOOKED || state === states.COMPLETED}
    >
      {driver}
    </TableCell>
    <TableCell
      className={classes.dateTableCell}
      show={state === states.BOOKED || state === states.COMPLETED}
    >
      {
        confirmedAt && moment(confirmedAt).format('LLL')
      }
    </TableCell>
    <TableCell
      className={classes.dateTableCell}
      show={state === states.COMPLETED}
    >
      {
        completedAt && moment(completedAt).format('LLL')
      }
    </TableCell>
  </Fragment>
);

Body.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  lot: PropTypes.shape({
    object: PropTypes.shape({
      points: PropTypes.arrayOf(PropTypes.shape({
        unloading: PropTypes.bool,
        date: PropTypes.string.isRequired,
        address: PropTypes.string.isRequired,
        note: PropTypes.string,
        company: PropTypes.string,
      })).isRequired,
    }).isRequired,
  }).isRequired,
  state: PropTypes.string.isRequired,
};

export default withStyles(styles)(Body);
