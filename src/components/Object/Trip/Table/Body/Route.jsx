/* eslint-disable react/no-typos */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import compose from 'recompose/compose';
import moment from 'moment';
import { observer, PropTypes as MobxPropTypes } from 'mobx-react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import grey from '@material-ui/core/colors/grey';

const BORDER_STYLE = `1px solid ${grey[400]}`;

const styles = {
  arrow: {
    cursor: 'default',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: '0 40px',
    maxWidth: 80,
    flex: 1,
  },
  line: {
    borderBottom: BORDER_STYLE,
    right: 0,
    top: '50%',
    flex: 2,
    minWidth: 16,
  },
  arrowhead: {
    borderTop: BORDER_STYLE,
    borderRight: BORDER_STYLE,
    height: 4,
    width: 4,
    transform: 'rotate(45deg)',
    marginLeft: -6,
  },
  leftLine: {
    flex: 4,
  },
  arrowBetween: {
    borderRadius: 10,
    border: BORDER_STYLE,
    color: grey[500],
    fontSize: 12,
    padding: '2px 8px',
    textAlign: 'center',
    whiteSpace: 'nowrap',
    flex: 1,
  },
  navigationNext: {
    color: grey[400],
    fontSize: 'inherit',
    marginRight: 8,
  },
  route: {
    fontSize: 'inherit !important',
  },
  point: {
    display: 'flex',
    flexDirection: 'column',
  },
};

const Route = ({
  classes,
  points,
  orders,
}) => {
  const route = (
    <Grid
      alignItems="center"
      className={classes.route}
      item
      container
      direction="row"
      justify="space-between"
      wrap="nowrap"
    >
      <div className={classes.point}>
        <Typography>{points[0].address}</Typography>
        <Typography variant="caption" noWrap>
          {moment(points[0].date).format('LLL')}
        </Typography>
      </div>
      {
        <span className={classes.arrow}>
          <span className={classNames(classes.line, classes.leftLine)} />
          {points.length > 2 && (
            <span className={classes.arrowBetween}>+{points.length - 2}</span>
          )}
          <span className={classes.line} />
          <span className={classes.arrowhead} />
        </span>
      }
      <div className={classes.point}>
        <Typography>
          {points[points.length - 1].address}
        </Typography>
        <Typography variant="caption" noWrap>
          {moment(points[points.length - 1].date).format('LLL')}
        </Typography>
      </div>
    </Grid>
  );

  return (
    <Grid container direction="column" spacing={8}>
      {route}
      <Grid item>
        <Typography variant="caption">
          {orders}
        </Typography>
      </Grid>
    </Grid>
  );
};

Route.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  points: MobxPropTypes.arrayOrObservableArrayOf(PropTypes.any).isRequired,
  orders: PropTypes.string,
};

Route.defaultProps = {
  orders: '',
};

export default compose(
  withStyles(styles),
  observer,
)(Route);
