import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import * as states from '../../../../constants/states';
import TableCell from '../../../TableCell';

const styles = {
  tableCell: {
  },
};

const Head = ({ classes, state }) => (
  <Fragment>
    <TableCell className={classes.tableCell}>Маршрут</TableCell>
    <TableCell numeric className={classes.tableCell}>Тоннаж</TableCell>
    <TableCell
      className={classes.tableCell}
      show={state === states.BOOKED || state === states.COMPLETED}
    >
      Водитель
    </TableCell>
    <TableCell
      className={classes.tableCell}
      show={state === states.BOOKED || state === states.COMPLETED}
    >
      Дата подтверждения
    </TableCell>
    <TableCell
      className={classes.tableCell}
      show={state === states.COMPLETED}
    >
      Дата завершения
    </TableCell>
  </Fragment>
);

Head.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  state: PropTypes.string.isRequired,
};

export default withStyles(styles)(Head);
