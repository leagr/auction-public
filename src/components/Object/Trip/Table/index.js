import Head from './Head';
import Body from './Body';

const Table = { Head, Body };

export default Table;
