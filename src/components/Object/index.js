import componentFromProp from 'recompose/componentFromProp';
import withProps from 'recompose/withProps';

import * as types from '../../constants/types';
import * as trip from './Trip';

const component = {
  [types.TRIP]: trip,
};

export const Table = {
  Head: withProps(({
    objectType,
  }) => ({
    component: component[objectType].Table.Head,
  }))(componentFromProp('component')),
  Body: withProps(({
    objectType,
  }) => ({
    component: component[objectType].Table.Body,
  }))(componentFromProp('component')),
};

export const Modal = {
  Body: withProps(({
    objectType,
  }) => ({
    component: component[objectType].Modal.Body,
  }))(componentFromProp('component')),
};

export const Filter = {
  Fields: withProps(({
    objectType,
  }) => ({
    component: component[objectType].Filter.Fields,
  }))(componentFromProp('component')),
  normalizer: objectType => component[objectType].Filter.normalizer,
  validationSchema: objectType => component[objectType].Filter.validationSchema,
  mapPropsToValues: objectType => component[objectType].Filter.mapPropsToValues,
};

export const Dialogs = {
  ConfirmLotDialog: objectType => component[objectType].Dialogs.ConfirmLotDialog,
  ConfirmInfoDialog: objectType => component[objectType].Dialogs.ConfirmInfoDialog,
};

export const Actions = {
  FileDownloadAction: withProps(({
    objectType,
  }) => ({
    component: component[objectType].Actions.FileDownloadAction,
  }))(componentFromProp('component')),
};
