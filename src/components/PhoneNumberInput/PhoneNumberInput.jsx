import React from 'react';
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';

const PHONE_NUMBER_FORMAT = '# (###) ###-##-##';

const PhoneNumberInput = ({
  inputRef,
  onChange,
  ...rest
}) => (
  <NumberFormat
    {...rest}
    format={PHONE_NUMBER_FORMAT}
    onValueChange={({ value }, e) => {
      e.target.value = value;
      onChange(e);
    }}
    ref={inputRef}
  />
);

PhoneNumberInput.propTypes = {
  inputRef: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default PhoneNumberInput;
