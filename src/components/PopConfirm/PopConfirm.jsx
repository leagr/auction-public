import React from 'react';
import PropTypes from 'prop-types';
import { compose, withStateHandlers } from 'recompose';
import {
  Button,
  Popover,
  Typography,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  actions: {
    display: 'flex',
    justifyContent: 'flex-end',
    width: '100%',
  },
  button: {
    flexGrow: 1,
  },
  container: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing.unit * 2,
  },
  title: {
    marginBottom: theme.spacing.unit * 2,
    width: '100%',
  },
});

const PopConfirm = ({
  anchorEl,
  cancelText,
  children,
  classes,
  handleClose,
  handleOpen,
  okText,
  // onCancel,
  onOk,
  title,
}) => (
  <React.Fragment>
    <Popover
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      classes={{
        paper: classes.container,
      }}
      open={Boolean(anchorEl)}
      onClose={handleClose}
    >
      <Typography align="left" className={classes.title}>
        {title}
      </Typography>
      <div className={classes.actions}>
        <Button
          className={classes.action}
          onClick={handleClose}
          size="small"
          color="primary"
        >
          {cancelText}
        </Button>
        <Button
          className={classes.action}
          onClick={() => { onOk(); handleClose(); }}
          size="small"
          color="primary"
        >
          {okText}
        </Button>
      </div>
    </Popover>
    {React.cloneElement(children, { onClick: (event) => { handleOpen(event.currentTarget); } })}
  </React.Fragment>
);

const enhance = withStateHandlers(
  {},
  {
    handleClose: () => () => ({ anchorEl: null }),
    handleOpen: () => target => ({ anchorEl: target }),
  },
);

PopConfirm.propTypes = {
  anchorEl: PropTypes.objectOf(PropTypes.any),
  cancelText: PropTypes.string,
  children: PropTypes.node.isRequired,
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  handleClose: PropTypes.func.isRequired,
  handleOpen: PropTypes.func.isRequired,
  okText: PropTypes.string,
  // onCancel: PropTypes.func.isRequired,
  onOk: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
};

PopConfirm.defaultProps = {
  anchorEl: null,
  cancelText: 'Ok',
  okText: 'Cancel',
};

export default compose(
  withStyles(styles),
  enhance,
)(PopConfirm);
