import React from 'react';
import PropTypes from 'prop-types';
import blue from '@material-ui/core/colors/blue';
import './Preloader.css';

const Preloader = ({ size, color, width }) => (
  <div className="preloader" style={{ width }}>
    <div
      className="preloader-inner"
      style={{ color, fontSize: size }}
    >
      <span>{'\u25cf'}</span>
      <span>{'\u25cf'}</span>
      <span>{'\u25cf'}</span>
      <span>{'\u25cf'}</span>
      <span>{'\u25cf'}</span>
      <span>{'\u25cf'}</span>
    </div>
  </div>
);

Preloader.propTypes = {
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  size: PropTypes.number,
  color: PropTypes.string,
};

Preloader.defaultProps = {
  size: 20,
  color: blue[500],
  width: 200,
};

export default Preloader;
