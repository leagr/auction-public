import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  text: {
    fontSize: 'inherit',
  },
};

const humanizePrice = (price) => {
  const mod = price.length % 3;

  let result = price.substr(0, mod);

  for (let i = mod; i <= price.length; i += 3) {
    result = `${result} ${price.substr(i, i + 3)}`;
  }

  return `${result} \u20bd`;
};

const Price = ({ price, color, classes }) => (
  <Typography className={classes.text} style={{ color }} noWrap>
    {humanizePrice(String(price))}
  </Typography>
);

Price.propTypes = {
  price: PropTypes.number.isRequired,
  color: PropTypes.string,
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
};

Price.defaultProps = {
  color: 'rgba(0, 0, 0, 0.87)',
};

export default withStyles(styles)(Price);
