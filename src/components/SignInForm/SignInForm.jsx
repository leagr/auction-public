import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import compose from 'recompose/compose';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';
import Yup from 'yup';
import { withFormik } from 'formik';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import CloseIcon from '@material-ui/icons/Close';

const styles = theme => ({
  paper: {
    width: 300,
    maxWidth: 300,
    padding: theme.spacing.unit * 3,
  },
  errorMessage: {
    backgroundColor: theme.palette.error.dark,
    height: 48,
    marginBottom: theme.spacing.unit * 3,
  },
  hidden: {
    visibility: 'hidden',
  },
});

const SignInForm = ({
  classes,
  values,
  error,
  closeError,
  handleChange,
  handleSubmit,
}) => (
  <React.Fragment>
    <SnackbarContent
      className={classNames(classes.errorMessage, { [classes.hidden]: !error })}
      action={(
        <IconButton color="inherit" onClick={closeError}>
          <CloseIcon />
        </IconButton>
      )}
      message={error}
    />
    <Paper className={classes.paper}>
      <form onSubmit={handleSubmit}>
        <Grid container direction="column" spacing={40}>
          <Grid item>
            <Typography variant="title" align="center">Авторизация</Typography>
          </Grid>
          <Grid item>
            <FormControl fullWidth>
              <InputLabel htmlFor="username">Имя пользователя</InputLabel>
              <Input
                id="username"
                value={values.username}
                onChange={handleChange}
              />
            </FormControl>
          </Grid>
          <Grid item>
            <FormControl fullWidth>
              <InputLabel htmlFor="password">Пароль</InputLabel>
              <Input
                id="password"
                type="password"
                value={values.password}
                onChange={handleChange}
              />
            </FormControl>
          </Grid>
          <Grid item>
            <Button type="submit" variant="raised" color="primary" fullWidth>
              Войти
            </Button>
          </Grid>
        </Grid>
      </form>
    </Paper>
  </React.Fragment>
);

SignInForm.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  values: PropTypes.shape({
    username: PropTypes.string,
    password: PropTypes.string,
  }).isRequired,
  error: PropTypes.string,
  closeError: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

SignInForm.defaultProps = {
  error: '',
};

const enhance = compose(
  withState('error', 'setError', ''),
  withHandlers({
    closeError: ({ setError }) => () => setError(''),
  }),
  withFormik({
    displayName: 'SignInForm',
    mapPropsToValues: () => ({ username: '', password: '' }),
    validationSchema: Yup.object().shape({
      username: Yup.string().required('Введите имя пользователя'),
      password: Yup.string().required('Введите пароль'),
    }),
    handleSubmit: (values, { props: { signIn, setError } }) => {
      signIn(values).then((result) => {
        if (result) {
          setError(result);
        }
      });
    },
  }),
  withStyles(styles),
);

export default enhance(SignInForm);
