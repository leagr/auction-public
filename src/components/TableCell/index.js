import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import setPropTypes from 'recompose/setPropTypes';
import mapProps from 'recompose/mapProps';
import defaultProps from 'recompose/defaultProps';
import branch from 'recompose/branch';
import renderNothing from 'recompose/renderNothing';
import BaseTableCell from '@material-ui/core/TableCell';

export default compose(
  setPropTypes({ show: PropTypes.bool.isRequired }),
  defaultProps({ show: true }),
  branch(
    ({ show }) => !show,
    renderNothing,
  ),
  mapProps(({ show, ...props }) => props),
)(BaseTableCell);
