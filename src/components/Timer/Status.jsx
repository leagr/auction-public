import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const Status = ({ classes, text, subText }) => (
  <div className={classNames(classes.container)}>
    <span className={classes.text}>{text}</span>
    <br />
    <span className={classes.subText}>{subText}</span>
  </div>
);

Status.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  text: PropTypes.string.isRequired,
  subText: PropTypes.string,
};

Status.defaultProps = {
  subText: '',
};

export default Status;
