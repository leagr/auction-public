import React from 'react';
import PropTypes from 'prop-types';
import {
  compose,
  withProps,
  branch,
  renderComponent,
} from 'recompose';
import classNames from 'classnames';
import { observer } from 'mobx-react';
import { withStyles } from '@material-ui/core/styles';
import PauseIcon from '@material-ui/icons/Pause';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import FastForwardIcon from '@material-ui/icons/FastForward';
import StopIcon from '@material-ui/icons/Stop';
import green from '@material-ui/core/colors/green';
import red from '@material-ui/core/colors/red';
import orange from '@material-ui/core/colors/orange';
import blue from '@material-ui/core/colors/blue';

import Status from './Status';

const styles = {
  container: {
    color: 'inherit',
    fontSize: 'inherit',
  },
  normal: {
    color: green[500],
  },
  pick: {
    color: blue[500],
  },
  icon: {
    fontSize: 'inherit',
  },
  last: {
    color: red[500],
  },
  wait: {
    color: orange[500],
  },
  timer: {
    alignItems: 'center',
    display: 'flex',
  },
  subText: {
    color: 'rgba(0, 0, 0, 0.54)',
    fontSize: '0.75rem',
  },
};

const normalize = (value) => {
  if (value < 10) {
    return `0${value}`;
  }

  return value;
};

const humanize = (rest) => {
  const h = Math.floor(rest / 3600);
  const m = Math.floor((rest - (h * 3600)) / 60);
  const s = Math.floor(rest - (h * 3600) - (m * 60));

  return `${normalize(h)}:${normalize(m)}:${normalize(s)}`;
};

const isLast = rest => rest < 60;

const Timer = ({
  classes,
  rest,
  isWait,
  isPick,
  isExtra,
  isNormal,
  isHot,
  isConfirm,
}) => (
  <div className={classes.container}>
    {
      <div className={classNames(
        classes.timer,
        {
          [classes.last]: isLast(rest),
          [classes.normal]: !isLast(rest) && (isNormal || isExtra || isHot || isConfirm),
          [classes.wait]: isWait,
          [classes.pick]: !isLast(rest) && isPick,
        },
      )}
      >
        {isWait && <PauseIcon className={classes.icon} />}
        {isNormal && <PlayArrowIcon className={classes.icon} />}
        {(isExtra || isHot) && <FastForwardIcon className={classes.icon} />}
        {isPick && <StopIcon className={classes.icon} />}
        {humanize(rest)}
      </div>
    }
  </div>
);

Timer.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  rest: PropTypes.objectOf(PropTypes.any).isRequired,
  isWait: PropTypes.bool.isRequired,
  isNormal: PropTypes.bool.isRequired,
  isExtra: PropTypes.bool.isRequired,
  isPick: PropTypes.bool.isRequired,
  isHot: PropTypes.bool.isRequired,
  isConfirm: PropTypes.bool.isRequired,
};

export default compose(
  withStyles(styles),
  withProps(({ lot }) => ({
    isWait: lot.isWait,
    isPick: lot.isPick,
    isExtra: lot.isExtra,
    isHot: lot.isHot,
    isNormal: lot.isNormal,
    isConfirm: lot.isConfirm,
    rest: lot.rest,
    isConfirmed: lot.isConfirmed,
    manualBooked: lot.manualBooked,
  })),
  branch(
    ({ manualBooked }) => manualBooked,
    branch(
      ({ isConfirmed }) => isConfirmed,
      compose(
        withProps(({ classes }) => ({
          text: 'Подтвержден',
          subText: 'Авто-бронирование',
          classes: {
            ...classes,
            text: classes.normal,
          },
        })),
        renderComponent(Status),
      ),
      compose(
        withProps(({ classes }) => ({
          text: 'Ожидает подтверждения',
          subText: 'Авто-бронирование',
          classes: {
            ...classes,
            text: classes.last,
          },
        })),
        renderComponent(Status),
      ),
    ),
  ),
  branch(
    ({ isConfirmed }) => isConfirmed,
    compose(
      withProps(({ classes }) => ({
        text: 'Подтвержден',
        classes: {
          ...classes,
          text: classes.normal,
        },
      })),
      renderComponent(Status),
    ),
  ),
  observer,
)(Timer);
