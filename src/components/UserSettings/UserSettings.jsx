import React from 'react';
import PropTypes from 'prop-types';
import { withFormik } from 'formik';
import Yup from 'yup';
import { withStyles } from '@material-ui/core/styles';
import {
  Card,
  CardHeader,
  CardContent,
  Grid,
  Typography,
  Button,
  FormControl,
  TextField,
} from '@material-ui/core';

const styles = theme => ({
  item: {
    minHeight: 108,
  },
  root: {
    position: 'relative',
    left: 'calc(50% - 250px)',
    padding: theme.spacing.unit * 3,
  },
  card: {
    width: 500,
  },
});

const UserSettings = ({
  classes,
  values,
  errors,
  touched,
  handleChange,
  handleSubmit,
}) => (
  <div className={classes.root}>
    <Card className={classes.card}>
      <CardHeader
        title={<Typography variant="subheading">Персональная информация</Typography>}
      />
      <CardContent style={{ flex: 1 }}>
        <Grid container direction="column">
          <Grid item className={classes.item}>
            <FormControl fullWidth>
              <TextField
                fullWidth
                id="employerSurname"
                label="Фамилия"
                value={values.employerSurname || ''}
                onChange={handleChange}
                error={Boolean(touched.employerSurname && errors.employerSurname)}
                helperText={touched.employerSurname && errors.employerSurname}
              />
            </FormControl>
          </Grid>
          <Grid item className={classes.item}>
            <FormControl fullWidth>
              <TextField
                fullWidth
                id="employerName"
                label="Имя"
                value={values.employerName || ''}
                onChange={handleChange}
                error={Boolean(touched.employerName && errors.employerName)}
                helperText={touched.employerName && errors.employerName}
              />
            </FormControl>
          </Grid>
          <Grid item className={classes.item}>
            <FormControl fullWidth>
              <TextField
                fullWidth
                id="employerPatronymic"
                label="Отчество"
                value={values.employerPatronymic || ''}
                onChange={handleChange}
                error={Boolean(touched.employerPatronymic && errors.employerPatronymic)}
                helperText={touched.employerPatronymic && errors.employerPatronymic}
              />
            </FormControl>
          </Grid>
          <Grid item className={classes.item}>
            <FormControl fullWidth>
              <TextField
                fullWidth
                id="phoneNumber"
                label="Контактный комер"
                value={values.phoneNumber || ''}
                onChange={handleChange}
                error={Boolean(touched.phoneNumber && errors.phoneNumber)}
                helperText={touched.phoneNumber && errors.phoneNumber}
              />
            </FormControl>
          </Grid>
          <Grid item className={classes.item}>
            <FormControl fullWidth>
              <TextField
                fullWidth
                id="companyName"
                label="Название компании"
                value={values.companyName || ''}
                onChange={handleChange}
                error={Boolean(touched.companyName && errors.companyName)}
                helperText={touched.companyName && errors.companyName}
              />
            </FormControl>
          </Grid>
          <Grid container direction="row" item justify="flex-end">
            <Button
              className={classes.button}
              color="primary"
              variant="raised"
              onClick={handleSubmit}
            >
              Сохранить
            </Button>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  </div>
);

UserSettings.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  values: PropTypes.shape({
    employerName: PropTypes.string,
    employerSurname: PropTypes.string,
    employerPatronymic: PropTypes.string,
    companyName: PropTypes.string,
    phoneNumber: PropTypes.string,
  }).isRequired,
  handleChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  errors: PropTypes
    .objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.bool]))
    .isRequired,
  touched: PropTypes.objectOf(PropTypes.bool).isRequired,
};

const EnhancedUserSettings = withFormik({
  displayName: 'UserSettings',
  mapPropsToValues: ({ userInfo }) => userInfo,
  validationSchema: Yup.object().shape({
    employerName: Yup.string().required('Введите имя'),
    employerSurname: Yup.string().required('Введите фамилию'),
    employerPatronymic: Yup.string().required('Введите отчество'),
    companyName: Yup.string().required('Введите название компании'),
    phoneNumber: Yup.string().required('Введите номер телефона'),
  }),
  handleSubmit: (values, { props: { editUserInfo } }) => {
    editUserInfo(values);
  },
})(UserSettings);

export default withStyles(styles)(EnhancedUserSettings);
