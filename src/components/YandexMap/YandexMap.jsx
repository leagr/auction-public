import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';

import { libraryIsLoaded } from '../../core/utils';

const styles = {
  root: {
    display: 'flex',
    alignContent: 'center',
    justifyContent: 'center',
    width: '100%',
    height: 400,
  },
  map: {
    width: '100%',
    height: '100%',
  },
  hiddenMap: {
    width: 0,
    height: 0,
    margin: 0,
    padding: 0,
  },
};

class YandexMap extends Component {
  state = {
    isLoaded: libraryIsLoaded('ymaps'),
    isError: false,
  };

  componentDidMount() {
    this.load();
  }

  load = () => {
    const { isLoaded } = this.state;

    if (isLoaded) {
      this.renderMap();
    } else {
      const script = document.createElement('script');

      script.src = 'https://api-maps.yandex.ru/2.1/?lang=ru_RU';
      script.async = true;
      script.onload = () => {
        this.setState({ isLoaded: true });
        this.renderMap();
      };
      script.onerror = () => {
        this.setState({ isError: true });
      };

      document.body.appendChild(script);
    }
  };

  renderMap = () => {
    const { ymaps } = window;

    ymaps.ready(() => {
      const map = new ymaps.Map(this.map, {
        controls: [],
        center: [55.796289, 49.108795],
        zoom: 10,
      });

      this.renderRoute(map);
    });
  };

  renderRoute = (map) => {
    const { ymaps } = window;
    const { points } = this.props;

    const multiRoute = new ymaps.multiRouter.MultiRoute({
      referencePoints: points.map(point => point.address),
      params: {
        results: 1,
      },
    }, { boundsAutoApply: true });

    map.geoObjects.add(multiRoute);
  };

  render() {
    const { classes } = this.props;
    const { isError, isLoaded } = this.state;

    return (
      <div className={classes.root}>
        <div
          className={classNames(classes.map, {
            [classes.hiddenMap]: !isLoaded || isError,
          })}
          ref={(map) => { this.map = map; }}
        />
        {isError && <span>some error happened</span>}
      </div>
    );
  }
}

YandexMap.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  points: PropTypes.arrayOf(PropTypes.shape({
    address: PropTypes.string.isRequired,
  })).isRequired,
};

export default withStyles(styles)(YandexMap);
