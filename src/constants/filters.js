const TRIP_FILTERS = [
  'city_from',
  'city_to',
  'start_date',
  'end_date',
  'start_tonnage',
  'end_tonnage',
];

export const COMMON_FILTERS = [
  'state',
  'group_key',
  'order_number',
  'start_price',
  'end_price',
];

export const OBJECT_FILTERS = {
  trip: TRIP_FILTERS,
};
