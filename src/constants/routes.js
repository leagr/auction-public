export const SIGN_IN = '/signin';
export const AUCTION = '/auction';
export const ACCOUNT = '/account';
export const LOT = `${AUCTION}/:lotId(\\d+)`;
export const lot = lotID => `${AUCTION}/${lotID}`;
