import compose from 'recompose/compose';
import mapProps from 'recompose/mapProps';
import { withRouter } from 'react-router-dom';
import queryString from 'query-string';

export default compose(
  withRouter,
  mapProps(({ match, history, location }) => ({
    routeAdd(path) {
      history.push({ pathname: `${match.url}/${path}` });
    },
    routeBack() {
      history.goBack();
    },
    routeModify(path) {
      const arr = match.url.split('/');
      arr[arr.length - 1] = path;
      history.push(arr.join('/'));
    },
    routePush(path) {
      history.push({ pathname: path });
    },
    routeReplace(path) {
      history.replace(path);
    },
    routeAddQueryValues(values) {
      history.replace(`${match.url}?${queryString.stringify(values)}`);
    },
    get routePath() {
      return location.pathname;
    },
    get routePathValues() {
      return match.params;
    },
    get routeQueryValues() {
      return queryString.parse(location.search);
    },
    get routeUrl() {
      return match.url;
    },
  })),
);
