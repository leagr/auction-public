export const libraryIsLoaded = name => Boolean(window[name]);

export const checkPhoneNumber = (phoneNumber) => {
  if (!phoneNumber) {
    return false;
  }

  const value = phoneNumber.match(/\d/g);

  if (value) {
    return value.length === 11;
  }

  return false;
};

export const initPollyfils = () => {
  if (!String.prototype.endsWith) {
    // eslint-disable-next-line no-extend-native
    Object.defineProperty(String.prototype, 'endsWith', {
      value: (searchString, position) => {
        const subjectString = this.toString();
        let positionInner;

        if (position === undefined || position > subjectString.length) {
          positionInner = subjectString.length;
        }

        positionInner -= searchString.length;

        const lastIndex = subjectString.indexOf(searchString, positionInner);

        return lastIndex !== -1 && lastIndex === positionInner;
      },
    });
  }
};
