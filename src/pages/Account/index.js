import { inject } from 'mobx-react';

import userSettings from '../../components/UserSettings';

export default inject(({
  sessionStore: { userInfo, editUserInfo },
}) => ({
  userInfo,
  editUserInfo,
}))(userSettings);
