import React from 'react';
import { inject } from 'mobx-react';
import Error from '../../components/Error';
import Filter from './Filter';
import Layout from './Layout';
import Lots from './Lots';
import LotsHeader from './LotsHeader';

const AuctionError = inject(({ errorStore: { error, close, show } }) =>
  ({ error, handleClose: close, show }))(Error);

const Auction = () => (
  <React.Fragment>
    <Layout>
      <Layout.Header>
        <LotsHeader />
      </Layout.Header>
      <Layout.Filter>
        <Filter />
      </Layout.Filter>
      <Layout.List>
        <Lots />
      </Layout.List>
    </Layout>
    <AuctionError />
  </React.Fragment>
);

export default Auction;
