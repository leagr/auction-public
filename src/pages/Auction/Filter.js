import { inject } from 'mobx-react';
import compose from 'recompose/compose';
import withProps from 'recompose/withProps';

import Filter from '../../components/Filter';

export default compose(
  inject(({
    filterStore: {
      clearFilters,
      setFilters,
      filters,
    },
    sessionStore: { user: { objectType } },
  }) => ({
    clearFilters,
    setFilters,
    objectType,
    filters,
  })),
  withProps(({
    clearFilters,
    filters: { state, group_key: groupKey, ...filters },
    setFilters,
  }) => ({
    state,
    filters,
    onSubmit: setFilters,
    onClear: clearFilters,
  })),
)(Filter);
