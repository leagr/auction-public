import React, { Fragment } from 'react';
import compose from 'recompose/compose';
import macro from 'macro-components';
import withStyles from '@material-ui/core/styles/withStyles';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({
  container: {
    boxSizing: 'border-box',
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing.unit * 3,
    flexGrow: 1,
    width: '100%',
  },
  list: {
    display: 'flex',
    flexGrow: 1,
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    height: 0,
  },
  filter: {
    width: '100%',
  },
  header: {
    margin: '-24px -24px 24px -24px',
  },
});

/* eslint-disable react/prop-types */
export const Header = ({ children }) => (
  <Fragment>{children}</Fragment>
);

export const Filter = ({ children }) => (
  <Fragment>{children}</Fragment>
);

export const List = ({ children }) => (
  <Fragment>{children}</Fragment>
);

// eslint-disable-next-line no-shadow
const Layout = ({ Filter, Header, List }, { classes }) => (
  <div className={classes.container}>
    <div className={classes.header}>
      {Header}
    </div>
    <div className={classes.filter}>
      {Filter}
    </div>
    <Paper className={classes.list}>
      {List}
    </Paper>
  </div>
);
/* eslint-enable react/prop-types no-shadow */

export default compose(
  withStyles(styles),
  macro({ Filter, Header, List }),
)(Layout);
