import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';

import Lots from '../../components/Lots';

export default compose(
  withRouter,
  inject(({
    sessionStore: { user: { objectType }, checkUser, isConnected },
    filterStore: { state },
    lotsStore: {
      lots,
      isLoading,
      getLot,
      lotIsLoading,
    },
  }) => ({
    objectType,
    state,
    lots,
    isLoading,
    lotIsLoading,
    checkUser,
    isConnected,
    getLot,
  })),
  observer,
)(Lots);
