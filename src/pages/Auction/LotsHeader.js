import compose from 'recompose/compose';
import { inject } from 'mobx-react';

import LotsHeader from '../../components/LotsHeader';

export default compose(inject(({ filterStore, sessionStore }) => ({
  setFilter: filterStore.setFilter,
  filters: filterStore.filters,
  groups: sessionStore.user.groups,
})))(LotsHeader);
