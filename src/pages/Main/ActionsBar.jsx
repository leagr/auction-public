import compose from 'recompose/compose';
import withHandlers from 'recompose/withProps';
import { inject } from 'mobx-react';
import ActionsBar from '../../components/ActionsBar';

export default compose(
  inject('sessionStore'),
  withHandlers(({ sessionStore }) => ({
    username: sessionStore.user.username,
    sendFeedback: sessionStore.sendFeedback,
  })),
)(ActionsBar);
