import compose from 'recompose/compose';
import withHandlers from 'recompose/withProps';
import withRoute from 'react-router-dom/withRouter';
import { inject } from 'mobx-react';
import * as routes from '../../constants/routes';
import AppHeader from '../../components/AppHeader';

export default compose(
  withRoute,
  inject('sessionStore'),
  withHandlers(({ history, sessionStore }) => ({
    username: sessionStore.user.username,
    onHome: () => history.push({ pathname: routes.AUCTION }),
    onProfile: () => history.push({ pathname: routes.ACCOUNT }),
    onSignOut: () => sessionStore.signOut(),
  })),
)(AppHeader);
