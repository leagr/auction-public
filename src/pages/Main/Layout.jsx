import React, { Fragment } from 'react';
import macro from 'macro-components';
import { compose } from 'recompose';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  toolbar: theme.mixins.toolbar,
  container: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    flex: 1,
    display: 'flex',
    flexWrap: 'nowrap',
  },
});

/* eslint-disable react/prop-types */
export const Header = ({ children }) => (
  <Fragment>{children}</Fragment>
);

export const Content = ({ children }) => (
  <Fragment>{children}</Fragment>
);

export const ActionsBar = ({ children }) => (
  <Fragment>{children}</Fragment>
);

// eslint-disable-next-line no-shadow
const Layout = ({ Header, Content, ActionsBar }, { classes }) => (
  <div
    className={classes.container}
  >
    <div className={classes.toolbar}>
      {Header}
    </div>
    <div className={classes.content}>
      {Content}
    </div>
    {ActionsBar}
  </div>
);
/* eslint-enable react/prop-types */

export default compose(
  withStyles(styles),
  macro({ Header, Content, ActionsBar }),
)(Layout);
