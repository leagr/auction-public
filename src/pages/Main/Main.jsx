import React from 'react';
import Router from 'react-router-dom/BrowserRouter';
import Switch from 'react-router-dom/Switch';
import Redirect from 'react-router-dom/Redirect';
import Route from 'react-router-dom/Route';

import * as routes from '../../constants/routes';
import SignIn from '../SignIn';
import Auction from '../Auction';
import Account from '../Account';
import Layout from './Layout';
import Header from './Header';
import ActionsBar from './ActionsBar';
import withSession from './withSession';

const Private = withSession(() => (
  <Layout>
    <Layout.Header>
      <Header />
    </Layout.Header>
    <Layout.Content>
      <Route exact path="/" render={() => <Redirect to={routes.AUCTION} />} />
      <Route path={routes.AUCTION} component={Auction} />
      <Route exact path={routes.ACCOUNT} component={Account} />
    </Layout.Content>
    <Layout.ActionsBar>
      <ActionsBar />
    </Layout.ActionsBar>
  </Layout>
));

const Main = () => (
  <Router>
    <Switch>
      <Route path={routes.SIGN_IN} component={SignIn} />
      <Route component={Private} />
    </Switch>
  </Router>
);

export default Main;
