import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Preloader from '../../components/Preloader';

const styles = {
  container: {
    top: '50%',
    position: 'absolute',
    width: '100%',
  },
};

const PagePreloader = ({ classes }) => (
  <div className={classes.container}>
    <Preloader size={40} width="100%" />
  </div>
);

PagePreloader.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
};

export default withStyles(styles)(PagePreloader);
