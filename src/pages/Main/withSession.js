import { compose, branch, withProps, renderComponent } from 'recompose';
import { inject, observer } from 'mobx-react';
import { Redirect } from 'react-router';

import * as routes from '../../constants/routes';
import PagePreloader from './PagePreloader';

const withSession = compose(
  inject(({ sessionStore: { authorized, initializing } }) =>
    ({ authorized, initializing })),
  observer,
  branch(
    ({ initializing }) => initializing,
    renderComponent(PagePreloader),
    branch(
      ({ authorized }) => !authorized,
      compose(
        withProps({ to: routes.SIGN_IN }),
        renderComponent(Redirect),
      ),
    ),
  ),
);

export default withSession;
