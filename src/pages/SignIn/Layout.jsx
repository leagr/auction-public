import React, { Fragment } from 'react';
import { compose } from 'recompose';
import macro from 'macro-components';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

const styles = {
  container: {
    width: '100vw',
    height: '100vh',
  },
};

/* eslint-disable react/prop-types */
export const Form = ({ children }) => (
  <Fragment>{children}</Fragment>
);

// eslint-disable-next-line no-shadow
const Layout = ({ Form }, { classes }) => (
  <Grid
    className={classes.container}
    container
    justify="center"
    alignItems="center"
  >
    <Grid item>{Form}</Grid>
  </Grid>
);
/* eslint-enable react/prop-types */

export default compose(
  withStyles(styles),
  macro({ Form }),
)(Layout);
