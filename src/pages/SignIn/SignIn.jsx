import React from 'react';
import PropTypes from 'prop-types';
import SignInForm from '../../components/SignInForm';
import withSession from './withSession';
import Layout from './Layout';

const SignIn = ({ signIn }) => (
  <Layout>
    <Layout.Form>
      <SignInForm signIn={signIn} />
    </Layout.Form>
  </Layout>
);

SignIn.propTypes = {
  signIn: PropTypes.func.isRequired,
};

export default withSession(SignIn);
