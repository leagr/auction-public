import { compose, branch, withProps, renderComponent } from 'recompose';
import { inject, observer } from 'mobx-react';
import { Redirect } from 'react-router';
import * as routes from '../../constants/routes';

const withSession = compose(
  inject(({ sessionStore: { authorized, signIn } }) => ({ authorized, signIn })),
  observer,
  branch(
    ({ authorized }) => authorized,
    compose(
      withProps({ to: routes.AUCTION }),
      renderComponent(Redirect),
    ),
  ),
);

export default withSession;
