import { action, observable } from 'mobx';

class ErrorStore {
  @observable error = {};
  @observable show = false;

  constructor(api) {
    this.api = api;
    this.api.ws.errorSubscribe(this.update);
  }

  @action update = (error) => {
    this.error = error;
    this.show = true;
  }

  @action close = () => {
    this.show = false;
  }
}

export default ErrorStore;
