import { observable, computed, action, reaction } from 'mobx';
import * as states from '../constants/states';
import { COMMON_FILTERS, OBJECT_FILTERS } from '../constants/filters';

const STATE = 'state';
const GROUP_KEY = 'group_key';

class FilterStore {
  @observable filtersMap;

  constructor(sessionStore) {
    this.filtersMap = new Map();

    reaction(
      () => sessionStore.user,
      (user) => {
        if (user !== null) {
          this.userId = user.id;

          const { groups, objectType } = user;

          this.initFilters(objectType, groups);
        } else {
          this.filtersMap.clear();
        }
      },
    );
  }

  initFilters = (objectType, groups) => {
    const { userId } = this;

    [...COMMON_FILTERS, ...OBJECT_FILTERS[objectType]].forEach((filter) => {
      const value = localStorage.getItem(`filter_${userId}_${filter}`);

      switch (filter) {
        case GROUP_KEY: {
          if (value && groups.filter(group => group.key === value)[0].length !== 0) {
            this.setFilter(filter, value);
          } else {
            this.setFilter(filter, groups[0].key);
          }
          break;
        }
        case STATE: {
          if (value) {
            this.setFilter(filter, value);
          } else {
            this.setFilter(filter, states.ACTIVE);
          }
          break;
        }
        default: {
          if (value) {
            this.setFilter(filter, value);
          }
        }
      }
    });
  };

  @computed get state() {
    return this.filtersMap.get(STATE);
  }

  @computed get groupKey() {
    return this.filtersMap.get(GROUP_KEY);
  }

  @computed get filters() {
    return this.filtersMap.toJSON();
  }

  @action setFilter = (key, value) => {
    const { userId } = this;

    switch (key) {
      case GROUP_KEY: {
        this.clearFilters();
        this.filtersMap.set(key, value);
        localStorage.setItem(`filter_${userId}_${key}`, value);
        break;
      }
      case STATE: {
        this.clearFilters();
        this.filtersMap.set(key, value);
        localStorage.setItem(`filter_${userId}_${key}`, value);
        break;
      }
      default: {
        this.filtersMap.set(key, value);
        localStorage.setItem(`filter_${userId}_${key}`, value);
      }
    }
  };

  @action setFilters = (filters) => {
    const { userId } = this;

    this.filtersMap.merge(filters);
    Object.keys(filters).forEach((key) => {
      if (filters[key]) {
        localStorage[`filter_${userId}_${key}`] = filters[key];
      }
    });
  };

  @action clearFilters = () => {
    const { userId } = this;

    this.filtersMap.forEach((_, key) => {
      if (key !== GROUP_KEY && key !== STATE) {
        localStorage.removeItem(`filter_${userId}_${key}`);
      }
    });

    this.filtersMap.replace({
      group_key: this.filtersMap.get('group_key'),
      state: this.filtersMap.get('state'),
    });
  };

  @action removeFilters = () => {
    this.clearFilters();
  };
}

export default FilterStore;
