import { action, computed, observable, reaction } from 'mobx';
import moment from 'moment';
import sortBy from 'lodash/sortBy';

import * as states from '../constants/states';
import Lot from './models/Lot';

class LotsStore {
  @observable lotsMap;
  @observable currentLotId;
  @observable isLoading;
  @observable lotIsLoading;

  constructor(filtersStore, sessionStore, api) {
    this.isLoading = false;
    this.lotIsLoading = false;

    this.lotsMap = new Map();
    this.filtersStore = filtersStore;
    this.sessionStore = sessionStore;

    this.api = api;
    this.api.ws.getLotSubscribe(this.updateLot);
    this.api.ws.getLotsSubscribe(this.updateLots);
    this.api.ws.lotAddedSubscribe(this.updateLot);
    this.api.ws.lotChangedSubscribe(this.updateLot);
    this.api.ws.lotDeletedSubscribe(this.deleteLot);

    reaction(
      () => this.filtersStore.filters,
      (filters) => {
        if (Object.keys(filters).length > 0) {
          this.getLots(filters);
        }
      },
    );

    this.start = Math.round(moment.now() / 1000);
    this.rest = 0;
    this.timer = setInterval(this.tick, 1000);
  }

  @action
  tick = () => {
    this.lotsMap.forEach(lot => lot.tick());
    this.rest += 1;

    const expectedValue = Math.round(moment.now() / 1000);
    const diff = expectedValue - this.start - this.rest;

    if (diff > 0) {
      this.lotsMap.forEach(lot => lot.updateRest(lot.rest - diff));
      this.rest += diff;
    }
  };

  getLot = (id) => {
    this.lotIsLoading = true;

    this.api.ws.getLot({ lot_id: Number(id) });
  };

  getLots = (filters) => {
    this.isLoading = true;

    this.lotsMap.clear();

    this.api.ws.getLots(filters);
  };

  @computed get lots() {
    const arr = [];

    this.lotsMap.forEach((value) => {
      arr.push(value);
    });

    return sortBy(arr, ['timestampFirstPointDate', 'id']);
  }

  @action updateLot = (value) => {
    const { state, groupKey } = this.filtersStore;
    const { user: { id: userId } } = this.sessionStore;
    const lot = new Lot(value, this.sessionStore.user, this.api.ws);

    try {
      if (state === states.ACTIVE) {
        if (value.booked_at) {
          this.lotsMap.delete(value.id);

          return;
        }

        if (value.group_key === groupKey) {
          this.lotsMap.set(value.id, lot);
        }
      }

      if (state === states.BOOKED) {
        if (value.completed_at) {
          this.lotsMap.delete(value.id);

          return;
        }

        if (value.confirmed_at) {
          this.lotsMap.set(value.id, lot);

          return;
        }

        if (value.booked_at) {
          if (value.bet && value.bet.user_id === userId) {
            if (value.group_key === groupKey) {
              this.lotsMap.set(value.id, lot);

              return;
            }
          }
        }

        this.lotsMap.delete(value.id);
      }

      if (state === states.COMPLETED) {
        if (value.completed_at) {
          this.lotsMap.set(value.id, lot);
        }
      }
    } finally {
      this.lotIsLoading = false;
    }
  };

  @action updateLots = async (lots) => {
    const lotsMap = new Map();

    await lots.forEach((lot) => {
      lotsMap.set(lot.id, new Lot(lot, this.sessionStore.user, this.api.ws));
    });

    this.lotsMap = lotsMap;

    this.isLoading = false;
  };

  @action deleteLot = (lot) => {
    this.lotsMap.delete(lot.id);
  }
}

export default LotsStore;
