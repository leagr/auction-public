import { observable, computed, action } from 'mobx';
import jwtDecode from 'jwt-decode';
import moment from 'moment';

import User from './models/User';

class SessionStore {
  @observable initializing = false;
  @observable user = null;
  @observable isConnected = false;

  constructor(errorStore, api) {
    this.api = api;
    this.api.ws.editUserSubscribe(this.userEdited);

    // eslint-disable-next-line no-shadow
    this.api.ws.wsSubject.connectionStatus.subscribe(({ isConnected, action }) => {
      this.isConnected = isConnected;

      if (action === 'close') {
        this.signOut();

        return;
      }

      if (localStorage.getItem('token')) {
        const { exp } = jwtDecode(localStorage.getItem('token'));
        const now = moment.now() / 1000;

        if (exp - now <= 5) {
          this.signOut();
        }
      }
    });

    this.api.ws.systemInvalidToken(() => {
      errorStore.update({
        error: 'Время сессии истекло',
        type: 'system.invalid.token',
      });
      setTimeout(() => {
        errorStore.close();
        this.api.ws.wsSubject.setConnected(false);
      }, 3000);
    });
  }

  checkUser = (id) => {
    if (this.user) {
      return this.user.id === id;
    }

    return false;
  };

  sendFeedback = (message) => {
    this.api.ws.sendFeedback({ message });
  }

  @computed get authorized() {
    return Boolean(this.user);
  }

  @computed get userInfo() {
    const {
      employerName,
      employerSurname,
      employerPatronymic,
      companyName,
      phoneNumber,
    } = this.user;

    return {
      employerName,
      employerSurname,
      employerPatronymic,
      companyName,
      phoneNumber,
    };
  }

  @action editUserInfo = (data) => {
    const {
      employerName,
      employerSurname,
      employerPatronymic,
      companyName,
      phoneNumber,
    } = data;

    this.api.ws.editUser({
      employer_name: employerName,
      employer_surname: employerSurname,
      employer_patronymic: employerPatronymic,
      company_name: companyName,
      phone_number: phoneNumber,
    });
  };

  @action userEdited = (data) => {
    this.user.update(data);
  };

  @action init = async () => {
    this.initializing = true;

    const user = await this.api.getUser();

    if (user) {
      this.user = new User(user);
    }

    this.initializing = false;
  };

  @action signIn = async (credentials) => {
    const result = await this.api.signIn(credentials);

    if (typeof result === 'object') {
      this.user = new User(result);
      return '';
    }

    return result;
  };

  @action signOut = () => {
    this.api.signOut();
    this.user = null;
  }
}

export default SessionStore;
