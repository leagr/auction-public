import { observable } from 'mobx';
import moment from 'moment';

import * as rules from '../../constants/rules';

export default class Lot {
  constructor(lot, user, ws) {
    this.updateLot(lot);

    this.user = user;
    this.ws = ws;
  }

  get isPick() {
    return this.rule === rules.PICK;
  }

  get isExtra() {
    return this.rule === rules.EXTRA;
  }

  get isNormal() {
    return this.rule === rules.NORMAL;
  }

  get isWait() {
    return this.rule === rules.WAIT;
  }

  get isHot() {
    return this.rule === rules.HOT;
  }

  get isConfirm() {
    return this.rule === rules.CONFIRM;
  }

  get isBet() {
    return Boolean(this.bet) && this.bet.user_id === this.user.id;
  }

  get isEmptyCurrentPrice() {
    return !this.currentPrice;
  }

  get isConfirmed() {
    return Boolean(this.confirmedAt);
  }

  get isBooked() {
    return Boolean(this.bookedAt);
  }

  get isCompleted() {
    return Boolean(this.completedAt);
  }

  get hasComplete() {
    return Boolean(this.complete);
  }

  get hasBet() {
    return Boolean(this.bet);
  }

  get getOrders() {
    const { orders } = this.object;

    if (orders) {
      const baseValue = orders[0].consignee_name;

      return orders.slice(1).reduce((acc, order) =>
        `${acc}; ${order.consignee_name}`, baseValue);
    }

    return '';
  }

  get timestampFirstPointDate() {
    const { points } = this.object;
    const firstPoint = points[0];
    return moment(firstPoint.date).unix();
  }

  updateRest = (rest) => {
    this.rest.set(rest);
  }

  updateLot = ({
    id,
    object: { data },
    bet,
    bet_step: betStep,
    current_price: currentPrice,
    rule,
    rest,
    confirmed_at: confirmedAt,
    booked_at: bookedAt,
    completed_at: completedAt,
    user_price: userPrice,
    manual_booked: manualBooked,
    group_key: groupKey,
    confirm,
    complete,
    base_price: basePrice,
  }) => {
    this.id = id;
    this.object = data;
    this.bet = bet;
    this.betStep = betStep;
    this.currentPrice = currentPrice;
    this.rule = rule;
    this.rest = observable.box(rest);
    this.confirmedAt = confirmedAt;
    this.bookedAt = bookedAt;
    this.groupKey = groupKey;
    this.userPrice = userPrice;
    this.manualBooked = manualBooked;
    this.confirm = confirm;
    this.complete = complete;
    this.completedAt = completedAt;
    this.basePrice = basePrice;
  }

  setConfirm = (confirm) => {
    this.confirm = confirm;
  }

  setComplete = (complete) => {
    this.complete = complete;
  }

  placeBet = (value) => {
    this.ws.placeBet({ lot_id: this.id, value });
  }

  cancelBet = () => {
    this.ws.cancelBet({ lot_id: this.id });
  }

  confirmLot = (values) => {
    this.ws.confirmLot(values);
  }

  editConfirmation = (values) => {
    this.ws.editConfirmation(values);
  }

  deleteConfirmation = () => {
    this.ws.deleteConfirmation({ lot_id: this.id });
  }

  completeLot = (values) => {
    this.ws.completeLot(values);
  }

  tick = () => {
    if (this.rest > 0) {
      this.rest.set(this.rest.get() - 1);
    }
  }

  get driver() {
    if (this.confirm) {
      const { name, surname, patronymic } = this.confirm;

      return `${surname} ${name} ${patronymic}`;
    }

    return '';
  }
}
