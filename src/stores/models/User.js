/* eslint-disable camelcase */
export default class User {
  constructor(data) {
    this.update(data);
  }

  update = ({
    object_type,
    groups,
    id,
    username,
    employer_name,
    employer_surname,
    employer_patronymic,
    phone_number,
    company_name,
  }) => {
    const values = {};

    this.username = username;

    values.id = id;
    values.objectType = object_type;
    values.groups = groups;
    values.employerName = employer_name;
    values.employerSurname = employer_surname;
    values.employerPatronymic = employer_patronymic;
    values.phoneNumber = phone_number;
    values.companyName = company_name;

    Object.keys(values).forEach((key) => {
      const value = values[key];
      if (value) {
        this[key] = value;
      }
    });
  }
}
/* eslint-enable camelcase */
